/* eslint-disable */

var express = require('express');
var app = express();
var http = require('http');
var fs = require("fs");
var server = http.createServer(app);

app.use(express.static('shellcoding'));

app.post('/contact.html', function (req, res) {
	console.log("contact post");
	var container = '';
	var title, context;

	req.on('data', function (chunk) {
		container = chunk.toString('utf8');
		console.log(container);
		const len = container.split(' ', 2);
		title = container.substr(len[0].length + len[1].length + 2, len[0]);
		context = container.substr(len[0].length + len[1].length + parseInt(len[0], 10) + 3, len[1]);
		console.log('Title : ', title, '\nContext : ', context);
	});

	req.on('end', function () {
		res.send({
			'response': {
				title: title,
				context: context
			}
		});
		res.end();
	});
});

var port = process.env.PORT || 3000;

server.listen(port, () => {
	console.log(`Listening on port ${port}...`);
});
