/* eslint-disable */

var time15;
var countDownInterval15;

function startCounter15() {
    time15 = 15
    counterInitial15();
}

function counterInitial15() {
    document.getElementById('g-timeCounterNum').innerHTML = time15;
    countDownInterval15 = window.setInterval(countDown15, 1000);
}

function countDown15() {
    time15--;
    document.getElementById('g-timeCounterNum').innerHTML = time15;
    if (time15 == 0) {
        counterStop15();
    }
}

function counterStop15() {
    clearInterval(countDownInterval15);
}
