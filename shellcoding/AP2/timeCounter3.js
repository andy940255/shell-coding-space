/* eslint-disable */

var time3;
var countDownInterval3;

function startCounter3() {
    time3 = 3
    counterInitial3();
}

function counterInitial3() {
    document.getElementById('g-timeCounterNum').innerHTML = time3;
    countDownInterval3 = window.setInterval(countDown3, 1000);
}

function countDown3() {
    time3--;
    document.getElementById('g-timeCounterNum').innerHTML = time3;
    if (time3 == 0) {
        counterStop3();
    }
}

function counterStop3() {
    clearInterval(countDownInterval3);
}
