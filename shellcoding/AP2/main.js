/* eslint-disable */

$(document).ready(function() {
    sp_start();
});

// shuffle for random question
function shuffle(arr) {
    var i, j, temp;
    for (i = arr.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}

// get image name by it's path
function GetImgName(path) {
    var filename;
    if (path.indexOf("/") > 0) { //如果有包括"/" 從最後一個"/"位置+1開始擷取
        filename = path.substring(path.lastIndexOf("/") + 1, path.length);
    } else {
        filename = path;
    }
    return filename;
}