/* eslint-disable */

var g1_1_qArr = ['chemistry', 'cement', 'taipower', 'oil', 'stone', 'ship', 'plastic', 'mineral'];
var g1_1_ansArr = new Array(4);
var g1_1_q_leftArr = ['#g1-1-qImg-1', '#g1-1-qImg-2'];
var g1_1_q_rightArr = ['#g1-1-qImg-4', '#g1-1-qImg-3'];

//下方選項的陣列 用於上鎖解鎖
var g1_1_selArr = ['#g1-1-sel-1', '#g1-1-sel-2', '#g1-1-sel-3', '#g1-1-sel-4', '#g1-1-sel-5'];

var g1_1_level;
var g1_1_correct_times;
var g1_1_level_grade;
var g1_1_save_time;
var g1_1_total_grade;

var startDiv = null; //drag and drop

function g1_1_start() {
    $('.game1-1').remove();

    g1_1_level = 0;
    g1_1_correct_times = 0;
    g1_1_level_grade = 0;
    g1_1_save_time = 0;

    // 洗g1-1的題目
    shuffle(g1_1_qArr);

    g1_1_load();

    // 開始g1(先顯示8個圖 然後開始g1-1)
    g1_show_all();
    //startCounter(3, 0, g1_remove_all, g1_1_start_levels);
    //數三秒(start)
    startCounter3();
    startG1 = setTimeout(function () {
        //三秒後
        g1_remove_all();
        g1_1_start_levels();

    }, 4000);
}

function g1_show_all() {
    $('#box').append('<img id="g1-1-chemistry" class="game1-1" src="image/game1-1/chemistry.png">');
    $('#box').append('<img id="g1-1-cement" class="game1-1" src="image/game1-1/cement.png">');
    $('#box').append('<img id="g1-1-taipower" class="game1-1" src="image/game1-1/taipower.png">');
    $('#box').append('<img id="g1-1-oil" class="game1-1" src="image/game1-1/oil.png">');
    $('#box').append('<img id="g1-1-stone" class="game1-1" src="image/game1-1/stone.png">');
    $('#box').append('<img id="g1-1-ship" class="game1-1" src="image/game1-1/ship.png">');
    $('#box').append('<img id="g1-1-plastic" class="game1-1" src="image/game1-1/plastic.png">');
    $('#box').append('<img id="g1-1-mineral" class="game1-1" src="image/game1-1/mineral.png">');
}

function g1_remove_all() {
    $('#g1-1-chemistry').remove();
    $('#g1-1-cement').remove();
    $('#g1-1-taipower').remove();
    $('#g1-1-oil').remove();
    $('#g1-1-stone').remove();
    $('#g1-1-ship').remove();
    $('#g1-1-plastic').remove();
    $('#g1-1-mineral').remove();
    //    $('#box').append('<img id="g1-1-donekey" class="game1-1" src="image/game1-1/donekey.png">');
    //    $('#box').append('<img id="g1-1-donekeycover" class="game1-1" src="image/game1-1/donekey.png">');
    $('#box').append('<img id="g1-1-bg-left" class="game1-1" src="image/game1-1/bg-left.png">');
    $('#box').append('<img id="g1-1-bg-right" class="game1-1" src="image/game1-1/bg-right.png">');
    $('#box').append('<img id="g1-1-bg-box-1" class="game1-1" src="image/game1-1/bg-box.png">');
    $('#box').append('<img id="g1-1-bg-box-2" class="game1-1" src="image/game1-1/bg-box.png">');
    $('#box').append('<img id="g1-1-bg-box-3" class="game1-1" src="image/game1-1/bg-box.png">');
    $('#box').append('<img id="g1-1-bg-box-4" class="game1-1" src="image/game1-1/bg-box.png">');
    $('#box').append('<div id="g1-1-sel-1" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-1" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    $('#box').append('<div id="g1-1-sel-2" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-2" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    $('#box').append('<div id="g1-1-sel-3" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-3" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    $('#box').append('<div id="g1-1-sel-4" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-4" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    $('#box').append('<div id="g1-1-sel-5" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-5" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');

}

function g1_1_load() {
    // bg
    $('#box').append('<img id="g1-1-bg" class="game1-1" src="image/game1-1/bg.png">');
    $('#box').append('<img id="g1-1-logo" class="game1-1" src="image/game1-1/logo.png">');
    $('#box').append('<img id="g1-1-bg-down" class="game1-1" src="image/game1-1/bg-down.png">');
    // random question * 4
    $('#box').append('<div id="g1-1-q-1" class="game1-1"><img id="g1-1-qImg-1" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-q-2" class="game1-1"><img id="g1-1-qImg-2" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-q-3" class="game1-1"><img id="g1-1-qImg-3" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-q-4" class="game1-1"><img id="g1-1-qImg-4" class="game1-1"></div>');
    // play's answer * 4
    $('#box').append('<div id="g1-1-ans-1" class="game1-1" data-check="0"  ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"></div>');
    $('#box').append('<div id="g1-1-ans-2" class="game1-1" data-check="0"  ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"></div>');
    $('#box').append('<div id="g1-1-ans-3" class="game1-1" data-check="0"  ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"></div>');
    $('#box').append('<div id="g1-1-ans-4" class="game1-1" data-check="0"  ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"></div>');
    // correct * 4
    $('#box').append('<div id="g1-1-correct-1" class="game1-1"><img id="g1-1-correctImg-1" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-correct-2" class="game1-1"><img id="g1-1-correctImg-2" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-correct-3" class="game1-1"><img id="g1-1-correctImg-3" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-correct-4" class="game1-1"><img id="g1-1-correctImg-4" class="game1-1"></div>');
    // wrong * 2
    $('#box').append('<div id="g1-1-wrong-1" class="game1-1"><img id="g1-1-wrongImg-1" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-wrong-2" class="game1-1"><img id="g1-1-wrongImg-2" class="game1-1"></div>');
    // time counter
    $('#box').append('<img id="g-timeCounter" class="game1-1" src="image/timecounter/counter.png">');
    $('#box').append('<div id="g-timeCounterNum" class="game1-1"></div>');
    // grade
    $('#box').append('<div id="g1-1-grade" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-standard" class="game1-1"></div>');
}

function g1_1_start_levels() {
    if (g1_1_level > 6) {
        g1_1_load_result();
        return;
    }
    g1_1_reset_image();
    g1_1_get_question(g1_1_level);
    g1_1_level += 2;

    document.getElementById('g1-1-grade').innerHTML = g1_1_level_grade;
    document.getElementById('g1-1-standard').innerHTML = 3000;

    //donekey
    $('#box').append('<img id="g1-1-donekey" class="game1-2" src="image/game1-2/donekey.png">');

    //15s start
    startCounter15();
    T1 = setTimeout(function () {
        //15秒後
        $('#g1-1-donekey').remove();
        g1_1_check();
        startCounter3();
        T2 = setTimeout(function () {
            g1_1_start_levels();
        }, 4000)

    }, 16000);


    $('#g1-1-donekey').on('click', function () {
        clearTimeout(T1);
        counterStop15();

        $('#g1-1-donekey').remove();
        g1_1_check();
        startCounter3();
        T2 = setTimeout(function () {
            g1_1_start_levels();
        }, 4000)

    });

}

function g1_1_get_question(i) {
    // random questions array by Back
    var g1_1_ans1 = g1_1_qArr[i];
    var g1_1_ans2 = g1_1_qArr[i + 1];
    g1_1_q_leftArr = ['#g1-1-qImg-1', '#g1-1-qImg-2'];
    g1_1_q_rightArr = ['#g1-1-qImg-4', '#g1-1-qImg-3'];
    shuffle(g1_1_q_leftArr);
    shuffle(g1_1_q_rightArr);
    $(g1_1_q_leftArr[0]).attr('src', 'image/game1-1/' + g1_1_ans1 + '-1.png');
    $(g1_1_q_leftArr[1]).attr('src', 'image/game1-1/' + g1_1_ans1 + '-2.png');
    $(g1_1_q_rightArr[0]).attr('src', 'image/game1-1/' + g1_1_ans2 + '-1.png');
    $(g1_1_q_rightArr[1]).attr('src', 'image/game1-1/' + g1_1_ans2 + '-2.png');
    g1_1_q_leftArr[0] = g1_1_q_leftArr[0].replace("qImg", "ans").replace("#", "");
    g1_1_q_leftArr[1] = g1_1_q_leftArr[1].replace("qImg", "ans").replace("#", "");
    g1_1_q_rightArr[0] = g1_1_q_rightArr[0].replace("qImg", "ans").replace("#", "");
    g1_1_q_rightArr[1] = g1_1_q_rightArr[1].replace("qImg", "ans").replace("#", "");

    // random selectors array
    var selArr = new Array(4);
    var random = Math.floor(Math.random() * 5);
    g1_1_ansArr = [g1_1_qArr[i], g1_1_qArr[i + 1], g1_1_qArr[(i + random + 2) % 8], g1_1_qArr[((i + 1 + random + 2) % 8)]];
    selArr = g1_1_ansArr.slice();
    shuffle(selArr);
    $('#g1-1-selImg-1').attr('src', 'image/game1-1/sel/' + selArr[0] + '.png');
    $('#g1-1-selImg-2').attr('src', 'image/game1-1/sel/' + selArr[1] + '.png');
    $('#g1-1-selImg-3').attr('src', 'image/game1-1/sel/' + selArr[2] + '.png');
    $('#g1-1-selImg-4').attr('src', 'image/game1-1/sel/' + selArr[3] + '.png');
    $('#g1-1-selImg-5').attr('src', 'image/game1-1/sel/idk1.png');
}

function g1_1_reset_image() {
    $('#g1-1-ans-1').remove();
    $('#g1-1-ans-2').remove();
    $('#g1-1-ans-3').remove();
    $('#g1-1-ans-4').remove();
    $('#g1-1-sel-1').remove();
    $('#g1-1-sel-2').remove();
    $('#g1-1-sel-3').remove();
    $('#g1-1-sel-4').remove();
    $('#g1-1-sel-5').remove();
    $('#g1-1-correct-1').remove();
    $('#g1-1-correct-2').remove();
    $('#g1-1-correct-3').remove();
    $('#g1-1-correct-4').remove();
    $('#g1-1-wrong-1').remove();
    $('#g1-1-wrong-2').remove();
    //    $('#g1-1-donekey').remove();
    //    $('#g1-1-donekeycover').remove();
    $('#test').remove();

    // play's answer * 4
    $('#box').append('<div id="g1-1-ans-1" class="game1-1" data-check="0" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"></div>');
    $('#box').append('<div id="g1-1-ans-2" class="game1-1" data-check="0" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"></div>');
    $('#box').append('<div id="g1-1-ans-3" class="game1-1" data-check="0" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"></div>');
    $('#box').append('<div id="g1-1-ans-4" class="game1-1" data-check="0" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"></div>');
    //  selector * 5
    $('#box').append('<div id="g1-1-sel-1" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-1" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    $('#box').append('<div id="g1-1-sel-2" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-2" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    $('#box').append('<div id="g1-1-sel-3" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-3" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    $('#box').append('<div id="g1-1-sel-4" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-4" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    $('#box').append('<div id="g1-1-sel-5" class="game1-1" data-check="1" ondragover="dragoverHandler(event)" ondrop="dropHandler(event)"><img id="g1-1-selImg-5" class="game1-1" draggable="true" ondragstart="dragstartHandler(event)" ></div>');
    // correct * 4
    $('#box').append('<div id="g1-1-correct-1" class="game1-1"><img id="g1-1-correctImg-1" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-correct-2" class="game1-1"><img id="g1-1-correctImg-2" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-correct-3" class="game1-1"><img id="g1-1-correctImg-3" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-correct-4" class="game1-1"><img id="g1-1-correctImg-4" class="game1-1"></div>');
    // wrong * 2
    $('#box').append('<div id="g1-1-wrong-1" class="game1-1"><img id="g1-1-wrongImg-1" class="game1-1"></div>');
    $('#box').append('<div id="g1-1-wrong-2" class="game1-1"><img id="g1-1-wrongImg-2" class="game1-1"></div>');
    //    // done key
    //    $('#box').append('<img id="g1-1-donekey" class="game1-1" src="image/game1-1/donekey.png">');
    //    $('#box').append('<img id="g1-1-donekeycover" class="game1-1" src="image/game1-1/donekey.png">');
}

// drag and drop
//開始拖曳圖片
function dragstartHandler(event) {
    event.dataTransfer.setData('image/jpg', event.currentTarget.id);
    var imgID = event.dataTransfer.getData('image/jpg'); //圖片ID
    var d = document.getElementById(imgID); //圖片物件
    var a = event.currentTarget; //***圖片物件***
    var notDivID = a.getAttribute('id'); //***圖片ID***
    var dad = a.parentElement; //***圖片原本的DIV***
    startDiv = dad;
    //當圖片離開div,check將變為0
    dad.setAttribute("data-check", 0);

    for (var sel = 0; sel < 5; sel++) {
        var objdiv = document.getElementById(g1_1_selArr[sel].replace("#", ""));
        var check = objdiv.getAttribute("data-check");

        if (check == 1) {
            objdiv.setAttribute("ondragover", "dragoverHandler(event)");
            objdiv.setAttribute("ondrop", "dropHandler(event)");
        }
    }
}

//圖片經過拖曳區
function dragoverHandler(event) {
    event.preventDefault();
}

//圖片放入拖曳區
function dropHandler(event) {
    event.preventDefault();
    var imgID = event.dataTransfer.getData('image/jpg'); //圖片ID
    var d = document.getElementById(imgID); //圖片物件
    var a = event.currentTarget; //目標DIV
    var dad = a.parentElement; //***這是box喔 神奇吧***
    var site = g1_1_q_leftArr.indexOf(a.id);
    var tmp = 0; //左邊
    if (site == -1) {
        site = g1_1_q_rightArr.indexOf(a.id);
        tmp = 1; //右邊
    }

    if (g1_1_selArr.indexOf("#" + startDiv.id) != -1 || g1_1_q_leftArr.indexOf(startDiv.id) != -1 || g1_1_q_rightArr.indexOf(startDiv.id) != -1) {
        if (a.childElementCount == 1) {
            startDiv.appendChild(a.childNodes[0]);
            startDiv.setAttribute("data-check", 1);
        } else if (a.childElementCount == 0) {
            if (site != -1) {
                if (tmp == 0)
                    tmp = document.getElementById(g1_1_q_leftArr[g1_1_swap_zero_one(site)]);
                else if (tmp == 1)
                    tmp = document.getElementById(g1_1_q_rightArr[g1_1_swap_zero_one(site)]);
                if (tmp.childElementCount == 1) {
                    startDiv.appendChild(tmp.childNodes[0]);
                    startDiv.setAttribute("data-check", 1);
                    tmp.setAttribute("data-check", 0);
                }
            }
        }
        a.appendChild(d);
        a.setAttribute("data-check", 1);
    }
    var imgName = GetImgName(d.src); // 移動中的圖片名字
    var divId = a.getAttribute('id'); // 移動到的div的ID
    startDiv = null; //初始化startDiv
    //sel lock
    for (var sel = 0; sel < 5; sel++) {
        var objdiv = document.getElementById(g1_1_selArr[sel].replace("#", ""));
        var check = objdiv.getAttribute("data-check");
        if (check == 0) {
            console.log(objdiv)
            objdiv.setAttribute("ondragover", "false");
            objdiv.setAttribute("ondrop", "false");
        }
    }
}

// 判斷是否正確
function g1_1_check() {
    g1_1_save_time += time15;

    //    // 鎖定不能拉
    //    for (var sel = 1; sel <= 5; sel++) {
    //        var objdiv = document.getElementById("g1-1-selImg-" + sel);
    //        objdiv.setAttribute("draggable", "false");
    //    }
    $('.game1-1').attr('draggable', 'false');

    //若 leftArr[0] (左邊正確的那個)答案正確  則圈  其他都錯
    var left_ImgName = null;
    var right_ImgName = null;
    var leftDiv = document.getElementById(g1_1_q_leftArr[0]);
    var rightDiv = document.getElementById(g1_1_q_rightArr[0]);

    if (leftDiv.getAttribute("data-check") != 0) {
        left_ImgName = GetImgName(((leftDiv).firstChild).getAttribute("src"));

        if (left_ImgName.localeCompare(g1_1_ansArr[0] + ".png") == 0) {
            g1_1_correct();
            if (g1_1_q_leftArr[0].charAt(9) == 1) {
                $('#g1-1-correct-1').css('z-index', '3');
                $('#g1-1-correctImg-1').attr('src', 'image/game1-1/correct.png');
            } else {
                $('#g1-1-correct-2').css('z-index', '3');
                $('#g1-1-correctImg-2').attr('src', 'image/game1-1/correct.png');
            }
        } else {
            $('#g1-1-wrong-1').css('z-index', '3');
            $('#g1-1-wrongImg-1').attr('src', 'image/game1-1/wrong.png');
        }
    } else {
        $('#g1-1-wrong-1').css('z-index', '3');
        $('#g1-1-wrongImg-1').attr('src', 'image/game1-1/wrong.png');
    }

    if (rightDiv.getAttribute("data-check") != 0) {
        right_ImgName = GetImgName(((rightDiv).firstChild).getAttribute("src"));

        if (right_ImgName.localeCompare(g1_1_ansArr[1] + ".png") == 0) {
            g1_1_correct();
            if (g1_1_q_rightArr[0].charAt(9) == 3) {
                $('#g1-1-correct-3').css('z-index', '3');
                $('#g1-1-correctImg-3').attr('src', 'image/game1-1/correct.png');
            } else {
                $('#g1-1-correct-4').css('z-index', '3');
                $('#g1-1-correctImg-4').attr('src', 'image/game1-1/correct.png');
            }
        } else {
            $('#g1-1-wrong-2').css('z-index', '3');
            $('#g1-1-wrongImg-2').attr('src', 'image/game1-1/wrong.png');
        }
    } else {
        $('#g1-1-wrong-2').css('z-index', '3');
        $('#g1-1-wrongImg-2').attr('src', 'image/game1-1/wrong.png');
    }
}

function g1_1_correct() {
    g1_1_correct_times++;
    g1_1_level_grade += 500;
}

function g1_1_load_result() {
    $('.game1-1').remove();

    $('#box').append('<img id="g1-1result-bg" class="game1-1" src="image/game1-1/result/bg.jpg">');
    $('#box').append('<div id="g1-1result-text-1" class="game1-1"></div>');
    $('#box').append('<div id="g1-1result-text-2" class="game1-1"></div>');
    $('#box').append('<div id="g1-1result-text-3" class="game1-1"></div>');
    $('#box').append('<div id="g1-1result-text-4" class="game1-1"></div>');

    var g1_1_time_grade = 0;
    if (g1_1_save_time >= 50) {
        g1_1_time_grade == 2000
    } else if (g1_1_save_time < 50 && g1_1_save_time >= 40) {
        g1_1_time_grade = 1500
    } else if (g1_1_save_time < 40 && g1_1_save_time >= 30) {
        g1_1_time_grade = 1000
    } else if (g1_1_save_time < 30 && g1_1_save_time >= 20) {
        g1_1_time_grade = 800
    } else if (g1_1_save_time < 20 && g1_1_save_time >= 10) {
        g1_1_time_grade = 400
    }

    var g1_1_total_grade = g1_1_level_grade + g1_1_time_grade;
    document.getElementById('g1-1result-text-1').innerHTML = "答對" + g1_1_correct_times + " 題 +" + g1_1_level_grade;
    document.getElementById('g1-1result-text-2').innerHTML = "剩下時間 +" + g1_1_time_grade;
    document.getElementById('g1-1result-text-3').innerHTML = "總共" + g1_1_total_grade + "分";

    if (g1_1_total_grade >= 3000) {
        document.getElementById('g1-1result-text-4').innerHTML = "恭喜挑戰成功 \n進入下一關";
        $('#box').append('<div id="g1-1result-next" class="game1-1">前往</div>');
    } else {
        document.getElementById('g1-1result-text-4').innerHTML = "喔不挑戰失敗";
        $('#box').append('<div id="g1-1result-restart" class="game1-1">重新</div>');
        $('#box').append('<div id="g1-1result-exit" class="game1-1">離開</div>');
    }

    $('#g1-1result-next').on('click', function () {
        $('.game1-1').remove();
        g1_2_start();
    })

    $('#g1-1result-restart').on('click', function () {
        $('.game1-1').remove();
        g1_1_start();
    })

    $('#g1-1result-exit').on('click', function () {
        $('.game1-1').remove();
        sp_start();
    })
}

function g1_1_swap_zero_one(num) {
    if (num) {
        return 0;
    } else {
        return 1;
    }
}
