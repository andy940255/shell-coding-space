/* eslint-disable */

var time, stopTime;
var countDownInterval, test;
var endfunc1, endfunc2;

function startCounter(startTime, func1, func2) {
    time = startTime
    endfunc1 = func1;
    endfunc2 = func2;
    console.log(endfunc1);
    console.log(endfunc2);
    counterInitial();
}

function counterInitial() {
    document.getElementById('g-timeCounterNum').innerHTML = time;
    countDownInterval = window.setInterval(countDown, 1000);
}

function countDown() {
    time--;
    document.getElementById('g-timeCounterNum').innerHTML = time;
    if (time == 0) {
        counterStop();
    }
}

function counterStop() {
    clearInterval(countDownInterval);
    endfunc1();
    setTimeout(endfunc2, 0);
}













