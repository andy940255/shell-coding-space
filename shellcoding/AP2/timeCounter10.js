/* eslint-disable */

var time10;
var countDownInterval10;

function startCounter10() {
    time10 = 10;
    counterInitial10();
}

function counterInitial10() {
    document.getElementById('g-timeCounterNum').innerHTML = time10;
    countDownInterval10 = window.setInterval(countDown10, 1000);
}

function countDown10() {
    time10--;
    document.getElementById('g-timeCounterNum').innerHTML = time10;
    if (time10 == 0) {
        counterStop10();
    }
}

function counterStop10() {
    clearInterval(countDownInterval10);
}
