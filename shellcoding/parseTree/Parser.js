/* eslint-disable */
//Parser class

function Parser(scanner) {
	this.scanner = scanner;
	this.currentToken = new Token();
	this.lookaheadToken = new Token();
	this.lookaheadToken.used = true;
	this.tabList = [0]; // 紀錄縮排層數
}

Parser.prototype.nextToken = function () {
	if (this.lookaheadToken.used) { // 若 lookaheadToken 被使用了
		do {
			var token = this.scanner.nextToken();
			//不是字串的狀況 一律略過空白
			while (token == Token.tokens.SPACE_TOKEN) {
				token = this.scanner.nextToken();
			}
			// skip comments and errors
		} while (token == Token.tokens.BLOCKCOMMENT_TOKEN || token == Token.tokens.HASHTAG_TOKEN || token == Token.tokens.ERROR_TOKEN);
		this.currentToken.type = this.scanner.currentToken.type;
		this.currentToken.text = this.scanner.currentToken.text;
		return token;
	} else { // looaheadToken 没有被使用
		this.currentToken.type = this.lookaheadToken.type;
		this.currentToken.text = this.lookaheadToken.text;
		this.lookaheadToken.used = true;
		return this.currentToken.type;
	}
}

Parser.prototype.lookahead = function () { // 先看下一個Token是什麼
	if (this.lookaheadToken.used) {
		do {
			var token = this.scanner.nextToken();
			//不是字串的狀況 一律略過空白
			while (token == Token.tokens.SPACE_TOKEN) {
				token = this.scanner.nextToken();
			}
			// skip comments and errors
		} while (token == Token.tokens.BLOCKCOMMENT_TOKEN || token == Token.tokens.HASHTAG_TOKEN || token == Token.tokens.ERROR_TOKEN);
		this.lookaheadToken.type = this.scanner.currentToken.type;
		this.lookaheadToken.text = this.scanner.currentToken.text;
		this.lookaheadToken.used = false;
		return token;
	} else { // 用過 lookahead 的話就直接回傳 lookaheadType
		return this.lookaheadToken.type;
	}
}

// the entry point of our parser
Parser.prototype.parse = function () {
	var rootBlock = new ExpressionBlockNode();
	this.parseExpressions(rootBlock, -1);
	return rootBlock;
}

// to parse a list of expressions
Parser.prototype.parseExpressions = function (expressionBlockNode, level) {
	while (this.lookahead() != Token.tokens.EOS_TOKEN && level <= this.tabList[this.tabList.length - 1]) {
		// skip \r \n Token
		if (this.lookahead() == Token.tokens.NEWLINE_TOKEN || this.lookahead() == Token.tokens.LEFTBRACE_TOKEN || this.lookahead() == Token.tokens.RIGHTBRACE_TOKEN || this.lookahead() == Token.tokens.SPACE_TOKEN) { // 重要!! 先略過 { 跟 }
			this.nextToken();
			continue;
		}
		if (this.tabList[this.tabList.length - 1] < this.scanner.lastTab) { // 如果有新的縮排  就加入
			this.tabList.push(this.scanner.lastTab);
		} else if (this.tabList[this.tabList.length - 1] > this.scanner.lastTab) { // 縮排減少  扣掉  並結束本次Parse(讓迴圈不會把迴圈外的程式碼納入其中)
			// this.tabList.length = (this.tabList.indexOf(this.scanner.lastTab) + 1); 錯誤示範
			this.tabList.length = this.tabList.length - 1; // 重要!!! 一次只能降一層 不然會導致跳多層的迴圈變為跳一層
			break;
		}
		if (this.lookahead() == Token.tokens.ELSE_TOKEN || this.lookahead() == Token.tokens.ELIF_TOKEN) { //遇 到 Else 或 Elif 就跳出  parseIfExpression內會另外處理
			break;
		}
		var expressionNode = this.parseExpression(); // Parse Tree
		if (expressionNode) {
			expressionBlockNode.push(expressionNode);
		}
	}
}
//to parse an expression 
Parser.prototype.parseExpression = function () { // 剖析目前的Token並交由各自的處理函式
	switch (this.lookahead()) {
		case Token.tokens.IMPORT_TOKEN:
		case Token.tokens.FROM_TOKEN:
			return this.parseImportExpression();
		case Token.tokens.IDENTIFIER_TOKEN: // a = 5+2
			return this.parseVarExpression();
		case Token.tokens.PRINT_TOKEN: // print
			return this.parsePrintExpression();
		case Token.tokens.IF_TOKEN: // if
			return this.parseIfExpression();
		case Token.tokens.WHILE_TOKEN: // while
			return this.parseWhileExpression();
		case Token.tokens.FOR_TOKEN: // for
			return this.parseForExpression();
		case Token.tokens.BREAK_TOKEN: // break
			this.nextToken();
			return new BreakNode(this.scanner.currLine);
		case Token.tokens.CONTINUE_TOKEN: // 重要 continue 尚未實踐
			this.nextToken();
			return new ContinueNode(this.scanner.currLine);
		default: // unexpected, consume it
			return this.parseCompoundExpression(0);
	}
}

Parser.prototype.parseImportExpression = function () {
	var nameCompound = new ImportNode(this.scanner.currLine); // 建立名稱的堆疊
	if (this.lookahead() == Token.tokens.FROM_TOKEN) { // 如果有 from
		this.nextToken(); // cosume from token
		this.nextToken(); // cosume IdentifierNode(source) token
		nameCompound.source = this.currentToken.text;
	}
	this.nextToken(); // cosume import  token

	while (this.lookahead() != Token.tokens.NEWLINE_TOKEN && this.lookahead() != Token.tokens.EOS_TOKEN) {
		// 取得Token
		this.nextToken();
		if (this.currentToken.type == Token.tokens.IDENTIFIER_TOKEN) {
			nameCompound.push(new IdentifierNode(this.currentToken.text, this.scanner.currLine));
		} else if (this.currentToken.type == Token.tokens.COMMA_TOKEN) {
			continue;
		} else if (this.currentToken.type == Token.tokens.AS_TOKEN) {
			var lastName = nameCompound.pop();
			this.nextToken();
			nameCompound.push(new AsNode(lastName.identifer, this.currentToken.text));
		} else {
			console.log(Token.backwardMap[this.currentToken.type]);
		}
	}

	return nameCompound;
}

// parse a = 5
Parser.prototype.parseVarExpression = function () {
	// cosume id token
	this.nextToken();
	var varName = this.currentToken.text;
	this.skipSpace();
	// = += -= *= /= %=
	var type = this.nextToken();
	return new VariableNode(varName, type, this.parseCompoundExpression(0), this.scanner.currLine);
}
Parser.prototype.parsePrintExpression = function () {
	// cosume print Token
	var printToken = this.nextToken();
	while ((this.lookahead() != Token.tokens.EOS_TOKEN) && (this.lookahead() != Token.tokens.NEWLINE_TOKEN)) {
		var c = this.nextToken();
		if (c == Token.tokens.LEFTPAREN_TOKEN) {
			var operand = this.parseCompoundExpression(0);
		}
	}
	return new PrintNode(operand, this.scanner.currLine);
}

Parser.prototype.parseConditionExpression = function () {
	var operand = this.parseCompoundExpression(0);
	return operand;
}

Parser.prototype.parseIfExpression = function () {
	// cosume if token
	this.nextToken();
	// 目前迴圈層數
	var level = this.tabList[this.tabList.length - 1];
	var condition = this.parseConditionExpression();
	var expressions = this.parseBlockExpression(level);
	var elifExpressions = new ExpressionBlockNode();
	var elseExpressions;

	// elif
	while ((this.lookahead() == Token.tokens.ELIF_TOKEN) && (level == this.scanner.lastTab)) {
		// cosume elif
		this.nextToken();
		var elifCondition = this.parseConditionExpression();
		// cosume :	Token
		this.nextToken();
		var elifExpression = this.parseBlockExpression(level);
		var elif = new ElifNode(elifCondition, elifExpression, this.scanner.currLine);
		elifExpressions.push(elif);
	}

	// else
	if ((this.lookahead() == Token.tokens.ELSE_TOKEN) && (level == this.tabList[this.tabList.length - 1])) {
		// cosume else Token
		this.nextToken();
		// cosume : Token
		this.nextToken();
		elseExpressions = this.parseBlockExpression(level);
	}
	return new IfNode(condition, expressions, elseExpressions, elifExpressions, this.scanner.currLine);
}

Parser.prototype.parseWhileExpression = function () {
	// cosume while token
	this.nextToken();
	// 目前迴圈層數
	var level = this.tabList[this.tabList.length - 1];
	var condition = this.parseConditionExpression();
	var expressions = this.parseBlockExpression(level);
	return new WhileNode(condition, expressions, this.scanner.currLine);
}

Parser.prototype.parseForExpression = function () {
	// cosume for Token
	this.nextToken();
	// 目前迴圈層數
	var level = this.tabList[this.tabList.length - 1];
	var compoundIdenitfier = new CompoundNode(this.scanner.currLine);
	// 處理變數部分(可能多個)
	if (this.lookahead() == Token.tokens.IDENTIFIER_TOKEN) {
		do {
			// cosume identifier Token
			this.nextToken();
			compoundIdenitfier.push(new IdentifierNode(this.currentToken.text, this.scanner.currLine));
		}
		while (this.lookahead() == Token.tokens.IDENTIFIER_TOKEN);
	} else { // 錯誤跳過
		this.skipError();
	}
	if (this.lookahead() == Token.tokens.IN_TOKEN) {
		//消耗 in Toekn
		this.nextToken();
	} else { // 錯誤跳過
		this.skipError();
	}
	if (this.lookahead() == Token.tokens.RANGE_TOKEN) {
		// 消耗 range Token
		this.nextToken();
		// 消耗 ( Token
		this.nextToken();
		var iteration = this.parseCompoundExpression(0);
		// 消耗 ) Token
		this.nextToken();
	} else {
		this.skipError();
	}
	var expressions = this.parseBlockExpression(level);
	return new ForNode(compoundIdenitfier, iteration, expressions);
}

Parser.prototype.parseParenExpression = function () {
	if (this.lookahead() != Token.tokens.LEFTPAREN_TOKEN) {
		console.log("Line " + this.scanner.currLine + ":(Syntax Error) Expecting a ( ");
	} else {
		// cosume ( token
		this.nextToken();
	}
	var expression = this.parseExpression();
	if (this.lookahead() != Token.tokens.RIGHTPAREN_TOKEN) {
		console.log("Line " + this.scanner.currLine + ":(Syntax Error) Expecting a ) ");
	} else {
		// cosume ) token
		this.nextToken();
	}
	return expression;
}

Parser.prototype.parseBlockExpression = function (level) {
	var block = new ExpressionBlockNode();
	var blockExpression = this.parseExpressions(block, level);
	return block;
}

Parser.prototype.parseOperand = function () { // 對複雜算式進行分類 例如 1 + 2 * (b * (4 / 6) - a) + 5 - -5
	// 重要 先略過空白
	this.skipSpace();
	var token = this.nextToken();
	switch (token) {
		case Token.tokens.INTLITERAL_TOKEN: // 整数
			return new IntNode(this.currentToken.text, this.scanner.currLine);
		case Token.tokens.BOOLLITERAL_TOKEN: // bool 
			return new BoolNode(this.currentToken.text, this.scanner.currLine);
		case Token.tokens.IDENTIFIER_TOKEN: // 變數
			var identifier = new IdentifierNode(this.currentToken.text, this.scanner.currLine);
			return identifier; // id
		case Token.tokens.LEFTPAREN_TOKEN: // 左圓括弧 （
			var operand = new ParenNode(this.parseCompoundExpression(0), this.scanner.currLine); // (....) 括弧裡面的內容
			if (this.lookahead() == Token.tokens.RIGHTPAREN_TOKEN) {
				// consume )
				this.nextToken();
			} else { //ERROR
				console.log("Line " + this.scanner.currLine + ":(Syntax Error) Expecting a ) ");
			}
			return operand;
		case Token.tokens.NOT_TOKEN: // not 較特別 所以可以獨立出来
			return new NotNode(this.parseOperand(), this.scanner.currLine);
		case Token.tokens.MINUS_TOKEN: // 負數
			return new NegateNode(this.parseOperand(), this.scanner.currLine);
		case Token.tokens.QUOTE_TOKEN: // '...'
			return new OperatorQuoteNode(this.currentToken.text, this.scanner.currLine);
		case Token.tokens.DOUBLEQUOTE_TOKEN: // "..."
			return new OperatorDoubleQuoteNode(this.currentToken.text, this.scanner.currLine);
		case Token.tokens.END_TOKEN: // print-end
			return new OperatorEndNode(this.currentToken.text, this.scanner.currLine);
		case Token.tokens.SEP_TOKEN: // print-sep
			return new OperatorSepNode(this.currentToken.text, this.scanner.currLine);
		case Token.tokens.SEMICOLON_TOKEN: // ; 表達式結束 
		case Token.tokens.COLON_TOKEN: // :
		case Token.tokens.RIGHTPAREN_TOKEN: // )
			return null;
		default: //ERROR
			console.log("Line " + this.scanner.currLine + ":(Syntax Error) Unexpected Token ", Token.backwardMap[token]);
			return null;
	}
}

Parser.prototype.parseCompoundExpression = function (rightBindingPower) { // 處理複雜之表達式
	var operandNode = this.parseOperand(); // 得到表達式的第一個操作符號是什麼
	if (operandNode == null) { // null表示錯誤或是表達式結束
		return null;
	}
	var compoundExpressionNode = new CompoundNode(this.scanner.currLine); // 建立符號的堆疊
	compoundExpressionNode.push(operandNode); // 操作符號Push
	var operator = this.lookahead(); // 獲取運算符號
	var leftBindingPower = this.getBindingPower(operator); // 獲取目前運算符號優先度
	if (leftBindingPower == -1) { // 不是一个運算符號  表達式結束
		return compoundExpressionNode; // 回傳堆疊
	}
	var c = 0;
	while (rightBindingPower < leftBindingPower) { // 當目前運算符號優先度大於下一個運算符號優先度才繼續
		operator = this.nextToken(); // 獲取下個運算符號
		compoundExpressionNode.push(this.createOperatorNode(operator)); // 運算符號Push
		var node = this.parseCompoundExpression(leftBindingPower);
		compoundExpressionNode.push(node);

		operator = this.lookahead(); // 接下来的操作符號
		leftBindingPower = this.getBindingPower(operator);
		if (leftBindingPower == -1) { // 不是運算符號 表達式結束
			return compoundExpressionNode;
		}
	}
	return compoundExpressionNode; // 回傳堆疊
}

Parser.prototype.getBindingPower = function (token) { // 運算符號優先度 數越高優先度越高
	switch (token) {
		case Token.tokens.POWER_TOKEN:
			return 210; // **
		case Token.tokens.MULT_TOKEN:
		case Token.tokens.DIV_TOKEN:
		case Token.tokens.MOD_TOKEN:
		case Token.tokens.DIVFLOOR_TOKEN:
			return 200; // * / % //
		case Token.tokens.QUOTE_TOKEN:
		case Token.tokens.DOUBLEQUOTE_TOKEN:
			return 195; // " '
		case Token.tokens.PLUS_TOKEN:
		case Token.tokens.MINUS_TOKEN:
			return 190; //+ -
		case Token.tokens.COMMA_TOKEN:
			return 185; // ,
		case Token.tokens.EQUAL_TOKEN:
		case Token.tokens.NOTEQUAL_TOKEN:
		case Token.tokens.GREATER_TOKEN:
		case Token.tokens.LESS_TOKEN:
		case Token.tokens.GREATEREQUAL_TOKEN:
		case Token.tokens.LESSEQUAL_TOKEN:
			return 180; // == != < > >= <=
		case Token.tokens.AND_TOKEN:
			return 170; // and
		case Token.tokens.OR_TOKEN:
			return 160; // or
		case Token.tokens.ASSIGN_TOKEN:
		case Token.tokens.MINUSASSIGN_TOKEN:
		case Token.tokens.PLUSASSIGN_TOKEN:
		case Token.tokens.MULTASSIGN_TOKEN:
		case Token.tokens.DIVASSIGN_TOKEN:
		case Token.tokens.DIVFLOORASSIGN_TOKEN:
		case Token.tokens.MODASSIGN_TOKEN:
			return 150; // = -= += *= /= %=
		case Token.tokens.SEMICOLON_TOKEN:
			return 0; // (
		default:
			return -1;
	}
}

Parser.prototype.createOperatorNode = function (operator) { //返回操作符node
	switch (operator) {
		case Token.tokens.PLUS_TOKEN:
			return new OperatorPlusNode(this.scanner.currLine);
		case Token.tokens.MINUS_TOKEN:
			return new OperatorMinusNode(this.scanner.currLine);
		case Token.tokens.MULT_TOKEN:
			return new OperatorMultNode(this.scanner.currLine);
		case Token.tokens.DIV_TOKEN:
			return new OperatorDivNode(this.scanner.currLine);
		case Token.tokens.MOD_TOKEN:
			return new OperatorModNode(this.scanner.currLine);
		case Token.tokens.DIVFLOOR_TOKEN:
			return new OperatorDivFloorNode(this.scanner.currLine);
		case Token.tokens.PLUSASSIGN_TOKEN:
			return new OperatorPlusAssighNode(this.scanner.currLine);
		case Token.tokens.MINUSASSIGN_TOKEN:
			return new OperatorMinusAssighNode(this.scanner.currLine);
		case Token.tokens.MULTASSIGN_TOKEN:
			return new OperatorMultAssighNode(this.scanner.currLine);
		case Token.tokens.DIVASSIGN_TOKEN:
			return new OperatorDivAssighNode(this.scanner.currLine);
		case Token.tokens.DIVFLOORASSIGN_TOKEN:
			return new OperatorDivFloorAssighNode(this.scanner.currLine);
		case Token.tokens.MODASSIGN_TOKEN:
			return new OperatorModAssighNode(this.scanner.currLine);
		case Token.tokens.AND_TOKEN:
			return new OperatorAndNode(this.scanner.currLine);
		case Token.tokens.OR_TOKEN:
			return new OperatorOrNode(this.scanner.currLine);
		case Token.tokens.EQUAL_TOKEN:
			return new OperatorEqualNode(this.scanner.currLine);
		case Token.tokens.NOTEQUAL_TOKEN:
			return new OperatorNotEqualNode(this.scanner.currLine);
		case Token.tokens.GREATER_TOKEN:
			return new OperatorGreaterNode(this.scanner.currLine);
		case Token.tokens.LESS_TOKEN:
			return new OperatorLessNode(this.scanner.currLine);
		case Token.tokens.GREATEREQUAL_TOKEN:
			return new OperatorGreaterEqualNode(this.scanner.currLine);
		case Token.tokens.LESSEQUAL_TOKEN:
			return new OperatorLessEqualNode(this.scanner.currLine);
		case Token.tokens.ASSIGN_TOKEN:
			return new OperatorAssignNode(this.scanner.currLine);
		case Token.tokens.POWER_TOKEN:
			return new OperatorPowerNode(this.scanner.currLine);
		case Token.tokens.COMMA_TOKEN:
			return new OperatorCommaNode(this.scanner.currLine);
		case Token.tokens.QUOTE_TOKEN:
			return new OperatorQuoteNode(this.currentToken.text, this.scanner.currLine);
		case Token.tokens.DOUBLEQUOTE_TOKEN:
			return new OperatorDoubleQuoteNode(this.currentToken.text, this.scanner.currLine);
		default:
			return null;
	}
}

Parser.prototype.skipError = function () {
	while (this.lookahead() != Token.tokens.NEWLINE_TOKEN && this.lookahead() != Token.tokens.EOS_TOKEN) {
		this.nextToken();
	}
}

Parser.prototype.skipSpace = function () {
	while (this.lookahead() == Token.tokens.SPACE_TOKEN) {
		this.nextToken();
	}
}
