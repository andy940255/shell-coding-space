/* eslint-disable */

function extend(subClass, baseClass) {
	function inheritance() {}
	inheritance.prototype = baseClass.prototype;

	subClass.prototype = new inheritance();
	subClass.prototype.constructor = subClass;
	subClass.baseConstructor = baseClass;
	subClass.superClass = baseClass.prototype;
}

function Node(param) {}

function ExpressionBlockNode() {
	ExpressionBlockNode.baseConstructor.call(this, "test");
	this.expressions = [];
}
extend(ExpressionBlockNode, Node);

ExpressionBlockNode.prototype.push = function (expression) {
	this.expressions.push(expression);
}
ExpressionBlockNode.prototype.iterate = function (func) {
	for (var i = 0, l = this.expressions.length; i < l; i++) {
		var expression = this.expressions[i];
		func(expression, i);
	}
}

function ImportNode(line) {
	this.fileNames = [];
	this.line = line;
}
extend(ImportNode, Node);

ImportNode.prototype.push = function (node) {
	this.fileNames.push(node);
}

ImportNode.prototype.pop = function () {
	return this.fileNames.pop();
}

function AsNode(bName, aName) {
	this.bName = bName;
	this.aName = aName;
}
extend(AsNode, Node);

function PrintNode(expressionNode, line) {
	this.expressionNode = expressionNode;
	this.line = line;
}
extend(PrintNode, Node);

function IntNode(data, line) {
	this.data = data;
	this.line = line;
}
extend(IntNode, Node);

function BoolNode(data, line) {
	this.data = data;
	this.line = line;
}
extend(BoolNode, Node);

function StringNode(data, line) {
	this.data = data;
	this.line = line;
}
extend(StringNode, Node);

function BreakNode(line) {
	this.line = line;
}
extend(BreakNode, Node);

function ContinueNode(line) {
	this.line = line;
}
extend(ContinueNode, Node);

function VariableNode(IdName, type, initExpressionNode, line) {
	this.IdName = IdName;
	this.type = type;
	this.initExpressionNode = initExpressionNode;
	this.line = line;
}
extend(VariableNode, Node);

function IfNode(conditionExpression, expressions, elseExpressions, elifExpressions, line) {
	this.conditionExpression = conditionExpression;
	this.elifExpressions = elifExpressions;
	this.expressions = expressions;
	this.elseExpressions = elseExpressions;
	this.line = line;
}
extend(IfNode, Node);

function ForNode(identifers, iteration, expressions) {
	this.identifers = identifers;
	this.iteration = iteration;
	this.expressions = expressions;
}
extend(ForNode, Node);

function WhileNode(conditionExpression, expressions, line) {
	this.conditionExpression = conditionExpression;
	this.expressions = expressions;
	this.line = line;
}
extend(WhileNode, Node);

function ElifNode(conditionExpression, expressions, line) {
	this.conditionExpression = conditionExpression;
	this.expressions = expressions;
	this.line = line;
}
extend(WhileNode, Node);

function IdentifierNode(identifer, line) {
	this.identifer = identifer;
	this.line = line;
}
extend(IdentifierNode, Node);

function ParenNode(node, line) {
	this.node = node;
	this.line = line;
}
extend(ParenNode, Node);

function NegateNode(node, line) {
	this.node = node;
	this.line = line;
}
extend(NegateNode, Node);

function CompoundNode(line) {
	this.nodes = [];
	this.line = line;
}
extend(CompoundNode, Node);

CompoundNode.prototype.push = function (node) {
	this.nodes.push(node);
}

CompoundNode.prototype.pop = function (node) {
	return this.nodes.pop();
}

CompoundNode.prototype.peek = function (node) {
	return this.nodes[this.nodes.length];
}

CompoundNode.prototype.isEmpty = function (node) {
	return this.nodes.length ? false : true;
}

function OperatorNode() {}
extend(OperatorNode, Node);

function OperatorPlusNode(line) {
	this.line = line;
}
extend(OperatorPlusNode, OperatorNode);

function OperatorMinusNode(line) {
	this.line = line;
}
extend(OperatorMinusNode, OperatorNode);

function OperatorMultNode(line) {
	this.line = line;
}
extend(OperatorMultNode, OperatorNode);

function OperatorDivNode(line) {
	this.line = line;
}
extend(OperatorDivNode, OperatorNode);

function OperatorDivFloorNode(line) {
	this.line = line;
}
extend(OperatorDivFloorNode, OperatorNode);

function OperatorModNode(line) {
	this.line = line;
}
extend(OperatorModNode, OperatorNode);

function OperatorPowerNode(line) {
	this.line = line;
}
extend(OperatorPowerNode, OperatorNode);

function OperatorCommaNode(line) {
	this.line = line;
}
extend(OperatorCommaNode, OperatorNode);

function OperatorQuoteNode(data, line) {
	this.data = data;
	this.line = line;
}
extend(OperatorQuoteNode, OperatorNode);

function OperatorDoubleQuoteNode(data, line) {
	this.data = data;
	this.line = line;
}
extend(OperatorDoubleQuoteNode, OperatorNode);

function OperatorEndNode(data, line) {
	this.data = data;
	this.line = line;
}
extend(OperatorEndNode, OperatorNode);

function OperatorSepNode(data, line) {
	this.data = data;
	this.line = line;
}
extend(OperatorSepNode, OperatorNode);

function OperatorAndNode(line) {
	this.line = line;
}
extend(OperatorAndNode, OperatorNode);

function OperatorOrNode(line) {
	this.line = line;
}
extend(OperatorOrNode, OperatorNode);

function OperatorEqualNode(line) {
	this.line = line;
}
extend(OperatorEqualNode, OperatorNode);

function NotNode(node, line) {
	this.node = node;
	this.line = line;
}
extend(NotNode, Node);

function OperatorNotEqualNode(line) {
	this.line = line;
}
extend(OperatorNotEqualNode, OperatorNode);

function OperatorGreaterNode(line) {
	this.line = line;
}
extend(OperatorGreaterNode, OperatorNode);

function OperatorLessNode(line) {
	this.line = line;
}
extend(OperatorLessNode, OperatorNode);

function OperatorGreaterEqualNode(line) {
	this.line = line;
}
extend(OperatorGreaterEqualNode, OperatorNode);

function OperatorLessEqualNode(line) {
	this.line = line;
}
extend(OperatorLessEqualNode, OperatorNode);

function OperatorAssignNode(line) {
	this.line = line;
}
extend(OperatorAssignNode, OperatorNode);

function OperatorPlusAssignNode(line) {
	this.line = line;
}
extend(OperatorPlusAssignNode, OperatorNode);

function OperatorMinusAssignNode(line) {
	this.line = line;
}
extend(OperatorMinusAssignNode, OperatorNode);

function OperatorMultAssignNode(line) {
	this.line = line;
}
extend(OperatorMultAssignNode, OperatorNode);

function OperatorDivAssignNode(line) {
	this.line = line;
}
extend(OperatorDivAssignNode, OperatorNode);

function OperatorDivFloorAssignNode(line) {
	this.line = line;
}
extend(OperatorDivFloorAssignNode, OperatorNode);

function OperatorModAssignNode(line) {
	this.line = line;
}
extend(OperatorModAssignNode, OperatorNode);
