/* eslint-disable */
//Analyser class

function Analyser() {
	this.vars = 'start\n';
}

Analyser.prototype.evaluateExpressionBlockNode = function (node) { // 遍歷ExpressionBlockNode
	if (!node) {
		return;
	}
	for (var i = 0; i < node.expressions.length; i++) {
		this.evaluateExpressionNode(node.expressions[i]);
	}
}

Analyser.prototype.evaluateExpressionNode = function (node) { // 個別處理ExpressionNode
	if (node instanceof VariableNode) {
		this.evaluateVariableNode(node);
	} else if (node instanceof PrintNode) {
		this.evaluatePrintNode(node);
	} else if (node instanceof ImportNode) {
		this.evaluateImportNode(node);
	} else if (node instanceof CompoundNode) {
		this.evaluateCompoundNode(node);
	} else if (node instanceof IdentifierNode) {
		this.evaluateIdentifierNode(node);
	} else if (node instanceof NegateNode) {
		this.evaluateNegateNode(node);
	} else if (node instanceof IntNode) {
		this.evaluateIntNode(node);
	} else if (node instanceof BoolNode) {
		this.evaluateBoolNode(node);
	} else if (node instanceof NotNode) {
		this.evaluateNotNode(node);
	} else if (node instanceof ParenNode) {
		this.evaluateParenNode(node);
	} else if (node instanceof OperatorQuoteNode) {
		this.evaluateQuoteNode(node);
	} else if (node instanceof OperatorDoubleQuoteNode) {
		this.evaluateDoubleQuoteNode(node);
	} else if (node instanceof OperatorNode) { // OperatorNode
		this.evaluateOperatorNode(node);
	} else if (node instanceof IfNode) {
		this.evaluateIfNode(node);
	} else if (node instanceof WhileNode) {
		this.evaluateWhileNode(node);
	} else if (node instanceof ForNode) {
		this.evaluateForNode(node);
	} else if (node instanceof BreakNode) {
		console.log("BreakNode");
	} else if (node instanceof ContinueNode) {
		console.log("ContinueNode");
	}
}

Analyser.prototype.evaluateImportNode = function (node) {
	if (node.source) {
		this.vars += ":from "
		this.vars += node.source;
		this.vars += " ";
	} else {
		this.vars += ":";
	}

	this.vars += "import ";
	var i = 0;

	while (node.fileNames.length > i) {
		var file = node.fileNames[i];
		if (file instanceof IdentifierNode) {
			this.vars += file.identifer;
		} else if (file instanceof AsNode) {
			this.vars += file.bName;
			this.vars += " as ";
			this.vars += file.aName;
		} else {
			console.log("import Error");
		}
		if (i < node.fileNames.length - 1) {
			this.vars += ", ";
		}
		i++;
	}
	this.vars += ";\n"
}

Analyser.prototype.evaluateIntNode = function (node) {
	this.vars += node.data;
}

Analyser.prototype.evaluateIdentifierNode = function (node) {
	this.vars += node.identifer;
}

Analyser.prototype.evaluateBoolNode = function (node) {
	this.vars += node.data;
}

Analyser.prototype.evaluateNegateNode = function (node) {
	this.vars += '-';
	if (node.node.data == undefined) {
		this.vars += node.node.identifer;
	} else {
		this.vars += node.node.data;
	}
}

Analyser.prototype.evaluateNotNode = function (node) {
	this.vars += 'not ';
	this.evaluateExpressionNode(node.node);
}

Analyser.prototype.evaluateParenNode = function (node) {
	this.vars += '( ';
	this.evaluateCompoundNode(node.node);
	this.vars += ' )'
}

Analyser.prototype.evaluateVariableNode = function (node) {
	this.vars += ':' + node.IdName;
	var token;
	if (node.type == Token.tokens.ASSIGN_TOKEN) { // =
		token = ' = ';
	} else if (node.type == Token.tokens.PLUSASSIGN_TOKEN) { //+=
		token = ' += ';
	} else if (node.type == Token.tokens.MINUSASSIGN_TOKEN) { // -=
		token = ' -= ';
	} else if (node.type == Token.tokens.MULTASSIGN_TOKEN) { // *=
		token = ' *= ';
	} else if (node.type == Token.tokens.DIVASSIGN_TOKEN) { // /=
		token = ' /= ';
	} else if (node.type == Token.tokens.MODASSIGN_TOKEN) { // %=
		token = ' %= ';
	} else if (node.type == Token.tokens.DIVFLOORASSIGN_TOKEN) { // //=
		token = ' //= ';
	}
	this.vars += token;
	this.evaluateExpressionNode(node.initExpressionNode);
	this.vars += ';\n';
}

Analyser.prototype.evaluatePrintNode = function (node) {
	this.vars += ':print(';
	this.evaluateCompoundNode(node.expressionNode);
	this.vars += ');\n';
}

Analyser.prototype.evaluateQuoteNode = function (node) {
	this.vars += node.data;
}

Analyser.prototype.evaluateDoubleQuoteNode = function (node) {
	this.vars += node.data;
}

Analyser.prototype.evaluateOperatorNode = function (node) {
	if (node instanceof OperatorPlusNode) { // +
		this.vars += ' + ';
	} else if (node instanceof OperatorMinusNode) { // -
		this.vars += ' - ';
	} else if (node instanceof OperatorMultNode) { // *
		this.vars += ' * ';
	} else if (node instanceof OperatorDivNode) { // /
		this.vars += ' / ';
	} else if (node instanceof OperatorDivFloorNode) { // //
		this.vars += ' // ';
	} else if (node instanceof OperatorModNode) { // %
		this.vars += ' % ';
	} else if (node instanceof OperatorPowerNode) { // **
		this.vars += ' ** ';

	} else if (node instanceof OperatorAndNode) { // and
		this.vars += ' and ';
	} else if (node instanceof OperatorOrNode) { // or
		this.vars += ' or ';
	} else if (node instanceof OperatorEqualNode) { // =
		this.vars += ' == ';
	} else if (node instanceof OperatorNotEqualNode) { // !=
		this.vars += ' != ';
	} else if (node instanceof OperatorGreaterNode) { // >
		this.vars += ' > ';
	} else if (node instanceof OperatorLessNode) { // <
		this.vars += ' < ';
	} else if (node instanceof OperatorGreaterEqualNode) { // >=
		this.vars += ' >= ';
	} else if (node instanceof OperatorLessEqualNode) { // <=
		this.vars += ' <= ';

	} else if (node instanceof OperatorCommaNode) { // ,
		this.vars += ', ';
	} else if (node instanceof OperatorEndNode) { // end
		this.vars += 'end = ' + node.data.substr(0, 1);
		this.vars += node.data.substr(1);
		this.vars += node.data.substr(0, 1) + ' ';
	} else if (node instanceof OperatorSepNode) { // sep
		this.vars += 'sep = ' + node.data.substr(0, 1);
		this.vars += node.data.substr(1);
		this.vars += node.data.substr(0, 1) + ' ';
	}
}

Analyser.prototype.evaluateIfNode = function (node) {
	var color = this.colorList();
	this.vars += '#' + color + ":**If Start **;\n";
	this.vars += 'if (';
	this.evaluateCompoundNode(node.conditionExpression);
	this.vars += '?) then (True)\n';
	this.evaluateExpressionBlockNode(node.expressions, 1);

	for (var i = 0; i < node.elifExpressions.expressions.length; i++) {
		this.vars += 'elseif (';
		this.evaluateCompoundNode(node.elifExpressions.expressions[i].conditionExpression);
		this.vars += '?) then (True)\n';
		this.evaluateExpressionBlockNode(node.elifExpressions.expressions[i].expressions);
	}
	if (node.elseExpressions != undefined) {
		this.vars += 'else (False) \n';
		this.evaluateExpressionBlockNode(node.elseExpressions);
	}
	this.vars += 'endif\n';
	this.vars += '#' + color + ":**If End **;\n";
}

Analyser.prototype.evaluateForNode = function (node) {
	/*
	range(1)
	range(1,2)
	range(1,2,)
	range(1,2,3)
	*/
	var color = this.colorList();
	this.vars += '#' + color + ":**For Start ( " + node.identifers.nodes[0].identifer + " )**;\n";
	if (node.iteration.nodes.length > 1) {
		var checkNegative = '<';
		var iterInt = 1;
		if (node.iteration.nodes[4] != null) { // 若未設定迭代間距則預設為1
			if (node.iteration.nodes[4].nodes[0] instanceof NegateNode) { // 迭代為負數
				console.log("Negate");
				checkNegative = ">";
				iterInt = '-' + node.iteration.nodes[4].nodes[0].node.data;
			} else { // 迭代為正數
				console.log("not Negate");
				iterInt = node.iteration.nodes[4].nodes[0].data;
			}
		}
		this.vars += ":" + node.identifers.nodes[0].identifer + " = " + node.iteration.nodes[0].data + ";\n";
		this.vars += "while(" + node.identifers.nodes[0].identifer + checkNegative + node.iteration.nodes[2].nodes[0].data + "?) is (True)\n";
		this.evaluateExpressionBlockNode(node.expressions, 1);

		this.vars += ":" + node.identifers.nodes[0].identifer + " += " + iterInt + ";\n";

	} else if (node.iteration.nodes.length == 1) {
		this.vars += ":" + node.identifers.nodes[0].identifer + " = 0;\n";
		this.vars += "while(" + node.identifers.nodes[0].identifer + " < " + node.iteration.nodes[0].data + "?) is (True)\n";
		this.evaluateExpressionBlockNode(node.expressions, 1);
		this.vars += ":" + node.identifers.nodes[0].identifer + " += 1;\n";
	}
	this.vars += 'endwhile (False)\n';
	this.vars += '#' + color + ':**For end ( ' + node.identifers.nodes[0].identifer + ' )**;\n';
}

Analyser.prototype.evaluateWhileNode = function (node) {
	var color = this.colorList();
	this.vars += '#' + color + ":**While Start **;\n";
	this.vars += 'while(';
	this.evaluateCompoundNode(node.conditionExpression);
	this.vars += '?) is (True)\n';
	this.evaluateExpressionBlockNode(node.expressions);
	this.vars += 'endwhile (False)\n';
	this.vars += '#' + color + ":**While End **;\n";
}

Analyser.prototype.evaluateCompoundNode = function (node) { // 遍歷CompoundNode
	if (node == null) return;
	for (var i = 0; i < node.nodes.length; i++) {
		var subNode = node.nodes[i];
		this.evaluateExpressionNode(subNode);
	}
}

Analyser.prototype.colorList = function () {
	var colorList = ['AliceBlue', 'AntiqueWhite', 'Aqua', 'Aquamarine', 'Azure', 'Beige', 'Bisque', 'BlanchedAlmond', 'BlueViolet', 'Brown', 'BurlyWood', 'CadetBlue', 'Chartreuse', 'Chocolate', 'Coral', 'CornflowerBlue', 'Crimson', 'Cyan', 'SkyBlue'];
	var color = Math.floor(Math.random() * colorList.length);
	return colorList[color];
}
