/* eslint-disable */
//Reader class
//str is the data to be read

/* 字詞處理 */
function Reader(str) {
	this.data = str;
	this.currPos = 0;
	this.length = str.length;
}

/* 獲取下一個字詞 */
Reader.prototype.nextChar = function () {
	if (this.currPos >= this.length) {
		return -1;
	}
	return this.data[this.currPos++];
}

/* 往回退一步 */
Reader.prototype.retract = function (n) {
	if (n === undefined) {
		n = 1;
	}
	this.currPos -= n;
	if (this.currPos < 0) {
		this.currPos = 0;
	}
}
