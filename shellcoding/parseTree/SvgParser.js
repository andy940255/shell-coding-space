/* eslint-disable */
//SvgParser class

function SvgParser() {
	this.varSvg = 'graph g {fontsize=10;    margin="0,0";    ranksep = .3;    nodesep = .65 node [fontname="helvetica", shape = box, style="rounded",fontsize=11, penwidth=1,color = dodgerblue,  fillcolor = cyan]; edge [fontname="Inconsolata, Consolas", fontsize=10]; \n MainNode[label="Main"]  \n';
	this.expCount = 0;
	this.idCount = 0;
	this.NodeCount = 0;
	this.List = [];
}

SvgParser.prototype.evaluateExpressionBlockNode = function (node, transfer, n) { // 遍歷ExpressionBlockNode
	var T = [];
	if (!node) {
		return;
	}
	for (var i = 0; i < node.expressions.length; i++) {
		var temp = this.evaluateExpressionNode(node.expressions[i], transfer);
		if (temp) {
			T.push(temp);
			if (n) {
				this.List.push(temp);
			}
		}
	}
	return T;
}

SvgParser.prototype.evaluateExpressionNode = function (node, transfer) { // 個別處理ExpressionNode
	if (node instanceof VariableNode) {
		return this.evaluateVariableNode(node, transfer);
	} else if (node instanceof PrintNode) {
		return this.evaluatePrintNode(node, transfer);
	} else if (node instanceof ImportNode) {
		return this.evaluateImportNode(node, transfer);
	} else if (node instanceof CompoundNode) {
		return this.evaluateCompoundNode(node, transfer);
	} else if (node instanceof IdentifierNode) {
		return this.evaluateIdentifierNode(node, transfer);
	} else if (node instanceof IntNode) {
		return this.evaluateIntNode(node, transfer);
	} else if (node instanceof BoolNode) {
		return this.evaluateBoolNode(node, transfer);
	} else if (node instanceof NegateNode) {
		return this.evaluateNegateNode(node, transfer);
	} else if (node instanceof NotNode) {
		return this.evaluateNotNode(node, transfer);
	} else if (node instanceof ParenNode) {
		return this.evaluateParenNode(node, transfer);
	} else if (node instanceof OperatorQuoteNode) {
		return this.evaluateQuoteNode(node, transfer);
	} else if (node instanceof OperatorDoubleQuoteNode) {
		return this.evaluateDoubleQuoteNode(node, transfer);
	} else if (node instanceof OperatorNode) { // OperatorNode
		return this.evaluateOperatorNode(node, transfer);
	} else if (node instanceof IfNode) {
		return this.evaluateIfNode(node, transfer);
	} else if (node instanceof WhileNode) {
		return this.evaluateWhileNode(node, transfer);
	} else if (node instanceof ForNode) {
		return this.evaluateForNode(node, transfer);
	} else if (node instanceof BreakNode) {
		var data = 'break' + (++this.idCount) + ' ';
		this.varSvg += data + '[label=break];  ';
		return data;
	} else if (node instanceof ContinueNode) {
		console.log("ContinueNode");
	}
	console.log("return :", transfer);
	return transfer;
}
SvgParser.prototype.evaluateImportNode = function (node) {
	var importNode = 'importNode' + (++this.NodeCount) + ' ';
	this.varSvg += importNode + '[label="import"];  ';

	if (node.source) {
		var fromNode = 'fromNode' + (++this.NodeCount) + ' ';
		this.varSvg += fromNode + '[label="from\n' + node.source + '"];  ';
		this.varSvg += importNode + '--' + fromNode + ' ';
	}

	var i = 0;
	while (node.fileNames.length > i) {
		var fileNode = 'fileNode' + (++this.NodeCount) + ' ';
		var file = node.fileNames[i];
		if (file instanceof IdentifierNode) {
			this.varSvg += fileNode + '[label="' + file.identifer + '"];  ';
		} else if (file instanceof AsNode) {
			this.varSvg += fileNode + '[label="' + file.bName + " as " + file.aName + '"];  ';
		} else {
			this.varSvg += fileNode + '[label="import Error"];  ';
			console.log("import Error");
		}
		i++;
		this.varSvg += importNode + '--' + fileNode + ' ';
	}
	return importNode;
}

SvgParser.prototype.evaluateIntNode = function (node) {
	var data = 'int' + (++this.idCount) + ' ';
	this.varSvg += data + '[label="' + node.data + '"];  ';
	return data;
}

SvgParser.prototype.evaluateIdentifierNode = function (node) {
	var data = 'id' + (++this.idCount) + ' ';
	this.varSvg += data + '[label="' + node.identifer + '"];  ';
	return data;
}

SvgParser.prototype.evaluateBoolNode = function (node) {
	var data = 'Bool' + (++this.idCount) + ' ';
	this.varSvg += data + '[label="' + node.data + '"];  ';
	return data;
}

SvgParser.prototype.evaluateNegateNode = function (node) {
	var data = 'Negate' + (++this.idCount) + ' ';
	if (node.node.data == undefined) {
		this.varSvg += data + '[label="' + '-' + node.node.identifer + '"];  ';
	} else {
		this.varSvg += data + '[label="' + '-' + node.node.data + '"];  ';
	}
	return data;
}

SvgParser.prototype.evaluateNotNode = function (node, transfer) {
	var data = 'Not' + (++this.idCount) + ' ';
	this.varSvg += data + '[label="not"]; ';
	var temp = this.evaluateExpressionNode(node.node, data);
	if (data != temp) {
		this.varSvg += data + '--' + temp + ' ';
	}
	return data;
}

SvgParser.prototype.evaluateParenNode = function (node, transfer) {
	var temp = this.evaluateCompoundNode(node.node, transfer);
	return transfer;
}

SvgParser.prototype.evaluateVariableNode = function (node, transfer) {
	var varNode = 'varNode' + (++this.NodeCount);
	this.varSvg += varNode + '[label="varNode"];  ';
	var idName = 'IdName' + (++this.NodeCount);
	this.varSvg += idName + '[label="' + node.IdName + '"];  ';
	this.varSvg += varNode + '--' + idName + ' ';
	var token;
	if (node.type == Token.tokens.ASSIGN_TOKEN) { // =
		token = ' = ';
	} else if (node.type == Token.tokens.PLUSASSIGN_TOKEN) { //+=
		token = ' += ';
	} else if (node.type == Token.tokens.MINUSASSIGN_TOKEN) { // -=
		token = ' -= ';
	} else if (node.type == Token.tokens.MULTASSIGN_TOKEN) { // *=
		token = ' *= ';
	} else if (node.type == Token.tokens.DIVASSIGN_TOKEN) { // /=
		token = ' /= ';
	} else if (node.type == Token.tokens.MODASSIGN_TOKEN) { // %=
		token = ' %= ';
	} else if (node.type == Token.tokens.DIVFLOORASSIGN_TOKEN) { // //=
		token = ' //= ';
	}
	var operatorNode = 'token' + (++this.NodeCount);
	var exp = 'exp' + (++this.expCount);
	this.varSvg += operatorNode + '[label="' + token + '"];  ';
	this.varSvg += varNode + '--' + operatorNode + ' ';
	this.varSvg += varNode + '--' + exp + ' ';
	var transfer = this.evaluateExpressionNode(node.initExpressionNode, exp);
	return varNode;
}

SvgParser.prototype.evaluatePrintNode = function (node, transfer) {
	var printNode = 'printNode' + (++this.NodeCount);
	this.varSvg += printNode + '[label="print"];  ';
	this.evaluateCompoundNode(node.expressionNode, printNode);
	return printNode;
}

SvgParser.prototype.evaluateQuoteNode = function (node, transfer) {
	var data = 'QuoteNode' + (++this.idCount) + ' ';
	this.varSvg += data + '[label="' + node.data + '"];  ';
	return data;
}

SvgParser.prototype.evaluateDoubleQuoteNode = function (node, transfer) {
	var data = 'DoubleQuoteNode' + (++this.idCount) + ' ';
	node.data = node.data.replace(/"([^"]*)"/g, "\\\"$1\\\"");
	this.varSvg += data + '[label="' + node.data + '"];  ';
	return data;
}

SvgParser.prototype.evaluateOperatorNode = function (node, transfer) {
	var operatorNode = 'OperatorNode' + (++this.NodeCount);
	if (node instanceof OperatorPlusNode) { // +
		this.varSvg += operatorNode + '[label="+"]; ';
	} else if (node instanceof OperatorMinusNode) { // -
		this.varSvg += operatorNode + '[label="-"]; ';
	} else if (node instanceof OperatorMultNode) { // *
		this.varSvg += operatorNode + '[label="*"]; ';
	} else if (node instanceof OperatorDivNode) { // /
		this.varSvg += operatorNode + '[label="/"]; ';
	} else if (node instanceof OperatorDivFloorNode) { // //
		this.varSvg += operatorNode + '[label="//"]; ';
	} else if (node instanceof OperatorModNode) { // %
		this.varSvg += operatorNode + '[label="%"]; ';
	} else if (node instanceof OperatorPowerNode) { // **
		this.varSvg += operatorNode + '[label="**"]; ';

	} else if (node instanceof OperatorAndNode) { // and
		this.varSvg += operatorNode + '[label="and"]; ';
	} else if (node instanceof OperatorOrNode) { // or
		this.varSvg += operatorNode + '[label="or"]; ';
	} else if (node instanceof OperatorEqualNode) { // =
		this.varSvg += operatorNode + '[label="=="]; ';
	} else if (node instanceof OperatorNotEqualNode) { // !=
		this.varSvg += operatorNode + '[label="!="]; ';
	} else if (node instanceof OperatorGreaterNode) { // >
		this.varSvg += operatorNode + '[label=">"]; ';
	} else if (node instanceof OperatorLessNode) { // <
		this.varSvg += operatorNode + '[label="<"]; ';
	} else if (node instanceof OperatorGreaterEqualNode) { // >=
		this.varSvg += operatorNode + '[label=">="]; ';
	} else if (node instanceof OperatorLessEqualNode) { // <=
		this.varSvg += operatorNode + '[label="<="]; ';
	} else if (node instanceof OperatorCommaNode) { // ,
		return transfer;
	} else if (node instanceof OperatorEndNode) { // end
		this.varSvg += operatorNode + '[label="end"]; ';
		var end = 'end' + (++this.NodeCount) + ' ';
		var temp = node.data.substr(0, 1).replace('"', '\\"');
		this.varSvg += end + '[label="' + temp + node.data.substr(1) + temp + '"]; ';
		this.varSvg += operatorNode + '--' + end + ' ';
	} else if (node instanceof OperatorSepNode) { // sep
		this.varSvg += operatorNode + '[label="sep"]; ';
		var sep = 'sep' + (++this.NodeCount) + ' ';
		var temp = node.data.substr(0, 1).replace('"', '\\"');
		this.varSvg += sep + '[label="' + temp + node.data.substr(1) + temp + '"]; ';
		this.varSvg += operatorNode + '--' + sep + ' ';
	}
	return operatorNode;
}

SvgParser.prototype.evaluateIfNode = function (node, transfer) {
	var ifNode = 'IfNode' + (++this.NodeCount);
	this.varSvg += ifNode + '[label="if"]; ';

	var IfConditionNode = 'IfConditionNode' + (++this.NodeCount);
	this.varSvg += IfConditionNode + '[label="if-condition"]; ';
	this.varSvg += ifNode + ' -- ' + IfConditionNode + ' ';

	var IfBodyNode = 'IfBodyNode' + (++this.NodeCount);
	this.varSvg += IfBodyNode + '[label="if-body"]; ';
	this.varSvg += ifNode + ' -- ' + IfBodyNode + ' ';
	this.evaluateCompoundNode(node.conditionExpression, IfConditionNode);
	var temp = this.evaluateExpressionBlockNode(node.expressions, IfBodyNode);
	for (var i = 0; i < temp.length; i++) {
		this.varSvg += IfBodyNode + ' -- ' + temp[i] + ' ';
	}
	for (var i = 0; i < node.elifExpressions.expressions.length; i++) {
		var elifNode = 'elifNode' + (++this.NodeCount);
		this.varSvg += elifNode + '[label="elif"]; ';
		this.varSvg += ifNode + ' -- ' + elifNode + ' ';
		var elifConditionNode = 'elifConditionNode' + (++this.NodeCount);
		this.varSvg += elifConditionNode + '[label="condition"]; ';
		this.varSvg += elifNode + ' -- ' + elifConditionNode + ' ';
		var elifbodyNode = 'elifbodyNode' + (++this.NodeCount);
		this.varSvg += elifbodyNode + '[label="body"]; ';
		this.varSvg += elifNode + ' -- ' + elifbodyNode + ' ';
		this.evaluateCompoundNode(node.elifExpressions.expressions[i].conditionExpression, elifConditionNode);
		temp = this.evaluateExpressionBlockNode(node.elifExpressions.expressions[i].expressions);
		for (var j = 0; j < temp.length; j++) {
			this.varSvg += elifbodyNode + ' -- ' + temp[j] + ' ';
		}
	}
	if (node.elseExpressions) {
		var elseBodyNode = 'elseBodyNode' + (++this.NodeCount);
		this.varSvg += elseBodyNode + '[label="else-body"]; ';
		this.varSvg += ifNode + ' -- ' + elseBodyNode + ' ';
		temp = this.evaluateExpressionBlockNode(node.elseExpressions);
		for (var i = 0; i < temp.length; i++) {
			this.varSvg += elseBodyNode + ' -- ' + temp[i] + ' ';
		}
	}
	return ifNode;
}

SvgParser.prototype.evaluateForNode = function (node, transfer) {
	/*
	range(1)
	range(1,2)
	range(1,2,)
	range(1,2,3)
	*/
	var forNode = 'forNode' + (++this.NodeCount);
	this.varSvg += forNode + '[label="for"]; ';
	if (node.iteration.nodes.length > 1) {
		var itemNode = 'itemNode' + (++this.NodeCount);
		this.varSvg += itemNode + '[label="item"]; ';
		this.varSvg += forNode + ' -- ' + itemNode + ' ';
		var item = 'item' + (++this.NodeCount);
		this.varSvg += item + '[label="item:' + node.identifers.nodes[0].identifer + '"]; ';
		this.varSvg += itemNode + ' -- ' + item + ' ';
		var iterInt;
		if (node.iteration.nodes[4] == null) { // 若未設定迭代間距則預設為1
			iterInt = 1;
		} else {
			iterInt = node.iteration.nodes[4].nodes[0].data;
		}
		var range = 'range' + (++this.NodeCount);
		this.varSvg += range + '[label="range:' + node.iteration.nodes[0].data + ' ~ ' + (node.iteration.nodes[2].nodes[0].data - 1) + ' \n step:' + iterInt + '"]; ';
		this.varSvg += itemNode + ' -- ' + range + ' ';
		var temp = this.evaluateExpressionBlockNode(node.expressions);
		for (var i = 0; i < temp.length; i++) {
			this.varSvg += forNode + ' -- ' + temp[i] + ' ';
		}
	} else if (node.iteration.nodes.length == 1) {
		var itemNode = 'itemNode' + (++this.NodeCount);
		this.varSvg += itemNode + '[label="item"]; ';
		this.varSvg += forNode + ' -- ' + itemNode + ' ';
		var item = 'item' + (++this.NodeCount);
		this.varSvg += item + '[label="item:' + node.identifers.nodes[0].identifer + '"]; ';
		this.varSvg += itemNode + ' -- ' + item + ' ';
		var iterInt = 1;
		var range = 'range' + (++this.NodeCount);
		this.varSvg += range + '[label="range: 0 ~ ' + (node.iteration.nodes[0].data - 1) + ' \n step:' + iterInt + '"]; ';
		this.varSvg += itemNode + ' -- ' + range + ' ';
		var temp = this.evaluateExpressionBlockNode(node.expressions);
		for (var i = 0; i < temp.length; i++) {
			this.varSvg += forNode + ' -- ' + temp[i] + ' ';
		}
	}
	return forNode;
}
SvgParser.prototype.evaluateWhileNode = function (node, transfer) {
	var whileNode = 'whileNode' + (++this.NodeCount);
	this.varSvg += whileNode + '[label="while"]; ';

	var whileConditionNode = 'IfConditionNode' + (++this.NodeCount);
	this.varSvg += whileConditionNode + '[label="condition"]; ';
	this.varSvg += whileNode + ' -- ' + whileConditionNode + ' ';

	var whileBodyNode = 'IfBodyNode' + (++this.NodeCount);
	this.varSvg += whileBodyNode + '[label="body"]; ';
	this.varSvg += whileNode + ' -- ' + whileBodyNode + ' ';

	this.evaluateCompoundNode(node.conditionExpression, whileConditionNode);

	var temp = this.evaluateExpressionBlockNode(node.expressions);
	for (var i = 0; i < temp.length; i++) {
		this.varSvg += whileBodyNode + ' -- ' + temp[i] + ' ';
	}
	return whileNode;
}

SvgParser.prototype.evaluateCompoundNode = function (node, transfer) { // 遍歷CompoundNode
	if (node == null) {
		var nullNode = 'nullNode' + (++this.NodeCount);
		this.varSvg += nullNode + '[label=""]; ';
		this.varSvg += transfer + ' -- ' + nullNode + ' ';
		return;
	}
	var node = this.infixToPrefix(node);
	this.parseNode(node.nodes.length - 1, node.nodes, transfer);
}

SvgParser.prototype.parseNode = function (i, nodes, transfer) {
	var c = 0;
	var operator = transfer;
	var subNode = nodes[i];
	if (i < 0) {
		return -100;
	}

	if (subNode instanceof IntNode || subNode instanceof BoolNode || subNode instanceof OperatorQuoteNode || subNode instanceof OperatorDoubleQuoteNode || subNode instanceof IdentifierNode || subNode instanceof NegateNode || subNode instanceof OperatorEndNode || subNode instanceof OperatorSepNode) {
		operator = this.evaluateExpressionNode(subNode, transfer);
		if (transfer != operator) {
			this.varSvg += transfer + '--' + operator + ' ';
		}
		return 0;
	} else if (!(subNode instanceof OperatorNode)) {
		operator = this.evaluateExpressionNode(subNode, transfer);
		if (transfer != operator) {
			this.varSvg += transfer + '--' + operator + ' ';
		}
		return 0;
	} else if (subNode instanceof OperatorNode) {
		c += 1
		operator = this.evaluateExpressionNode(subNode, transfer);
		if (transfer != operator) {
			this.varSvg += transfer + '--' + operator + ' ';
		}
		var left = this.parseNode(i - 1, nodes, operator);
		c += left;
		var right = this.parseNode(i - 2 * c, nodes, operator);
		c += right;
		return c;
	}
}

SvgParser.prototype.colorList = function () {
	var colorList = ['AliceBlue', 'AntiqueWhite', 'Aqua', 'Aquamarine', 'Azure', 'Beige', 'Bisque', 'BlanchedAlmond', 'BlueViolet', 'Brown', 'BurlyWood', 'CadetBlue', 'Chartreuse', 'Chocolate', 'Coral', 'CornflowerBlue', 'Crimson', 'Cyan', 'SkyBlue'];
	var color = Math.floor(Math.random() * colorList.length);
	return colorList[color];
}

SvgParser.prototype.infixToPrefix = function (node) {
	// 處理 中綴式 轉 前綴式
	var S1 = new CompoundNode(node.line);
	var S2 = new CompoundNode(node.line);

	if (node instanceof ParenNode) {
		node = node.node;
	}
	var length = node.nodes.length - 1;

	for (var i = length; i >= 0; i--) {
		var subNode = node.nodes[i];
		if (subNode instanceof OperatorEndNode || subNode instanceof OperatorSepNode) {
			S2.push(subNode);
		} else if (subNode instanceof IntNode || subNode instanceof BoolNode || subNode instanceof OperatorQuoteNode || subNode instanceof OperatorDoubleQuoteNode || subNode instanceof IdentifierNode || subNode instanceof NegateNode) {
			S2.push(subNode);
		} else if (subNode instanceof OperatorNode) {
			var loopCheck = 0;
			while (true && loopCheck++ < 100000) { // 小心變成無限迴圈  先防呆一下  100000次內
				if (S1.isEmpty() || S1.peek == ')') {
					S1.push(subNode);
					break;
				} else if (this.getBindingPower(subNode) >= this.getBindingPower(S1.peek)) {
					S1.push(subNode);
					break;
				} else {
					S2.push(S1.pop());
				}
			}
		} else if (subNode instanceof ParenNode || subNode instanceof CompoundNode) {
			var temp = this.infixToPrefix(subNode);
			for (var j = 0; j < temp.nodes.length; j++) {
				S2.push(temp.nodes[j]);
			}
		} else {
			S2.push(subNode);
		}
	}
	while (!S1.isEmpty()) {
		S2.push(S1.pop());
	}
	return S2;
}

SvgParser.prototype.getBindingPower = function (node) { // 運算符號優先度 數越高優先度越高
	if (node instanceof OperatorPowerNode) {
		return 210; // **
	} else if (node instanceof OperatorMultNode || node instanceof OperatorDivNode || node instanceof OperatorModNode || node instanceof OperatorDivFloorNode) {
		return 200; // * / % //
	} else if (node instanceof OperatorPlusNode || node instanceof OperatorMinusNode) {
		return 190; //+ -
	} else if (node instanceof OperatorEqualNode || node instanceof OperatorNotEqualNode || node instanceof OperatorGreaterNode || node instanceof OperatorLessNode || node instanceof OperatorGreaterEqualNode || node instanceof OperatorLessEqualNode) {
		return 180; // == != < > >= <=
	} else if (node instanceof OperatorAndNode) {
		return 170; // and
	} else if (node instanceof OperatorOrNode) {
		return 160; // or
	} else {
		return -1;
	}
}
