/* eslint-disable */
//Scanner class
//reader: the reader used to read in characters

function Scanner(reader) {
	this.isNewLine = true;
	this.lastTab = 0; // 目前縮排數量(層數)
	this.tabCount = 0; // 目前數了幾個 \t (空白)
	this.Tab = 0; // 單次縮排裡 有多少 \t (空白) 可能是多個 \t (空白)
	this.tabType = 0; // 縮排種類 0是\t  1是空白

	this.reader = reader;
	this.currentToken = new Token();
	this.currLine = 0;
	this.bufferStr = "";
	this.state = Scanner.START_STATE;
}
Scanner.START_STATE = 0; //初始狀態
Scanner.IDENTIFIER_STATE = Scanner.START_STATE + 1; //英文字or中文
Scanner.INTLITERAL_STATE = Scanner.IDENTIFIER_STATE + 1; //數字
Scanner.MULTISYMBOL_STATE = Scanner.INTLITERAL_STATE + 1; //複雜符號
Scanner.TAB_STATE = Scanner.MULTISYMBOL_STATE + 1; //TAB
Scanner.SLASH_STATE = Scanner.TAB_STATE + 1; //多行註解

Scanner.prototype.makeToken = function (type, text) {
	this.currentToken.type = type;
	this.currentToken.text = text;
	return type;
};

Scanner.prototype.nextToken = function () {
	switch (this.state) {
		case Scanner.START_STATE:
			var nextChar = this.reader.nextChar();
			if (this.isNewLine) {
				if (nextChar == ' ' || nextChar == '\t') {
					this.state = Scanner.TAB_STATE;
					this.tabCount += 1;
					return this.nextToken();
				} else {
					if (!this.Tab && this.tabCount > 0) { //當 Tab為0 且 tabCount大於0 才執行(第一次縮排結束)
						this.Tab = this.tabCount;
					}
					this.isNewLine = false;
					if (this.tabCount && this.Tab) {
						this.lastTab = Math.round(this.tabCount / this.Tab);
					} else {
						this.lastTab = 0;
					}
					this.tabCount = 0;
				}
			}

			//遇到字母開頭 可能是變數 或保留字 跳轉到識別狀態
			if ((nextChar >= 'a' && nextChar <= 'z') || (nextChar >= 'A' && nextChar <= 'Z')) {
				this.state = Scanner.IDENTIFIER_STATE;
				this.bufferStr = nextChar; //紀錄字首
				return this.nextToken();
			}
			//數字 跳轉到數字處理狀態
			if (nextChar >= '0' && nextChar <= '9') {
				this.state = Scanner.INTLITERAL_STATE;
				this.bufferStr = nextChar; //紀錄字首
				return this.nextToken();
			}
			switch (nextChar) {
				case undefined:
				case -1:
					return this.makeToken(Token.tokens.EOS_TOKEN);
				case ':':
					return this.makeToken(Token.tokens.COLON_TOKEN, ":");
				case ';':
					return this.makeToken(Token.tokens.SEMICOLON_TOKEN, ";");
				case ',':
					return this.makeToken(Token.tokens.COMMA_TOKEN, ";");
				case '.':
					return this.makeToken(Token.tokens.PERIOD_TOKEN, ";");
				case '(':
					return this.makeToken(Token.tokens.LEFTPAREN_TOKEN, "(");
				case ')':
					return this.makeToken(Token.tokens.RIGHTPAREN_TOKEN, ")");
				case '{':
					return this.makeToken(Token.tokens.LEFTBRACE_TOKEN, "{");
				case '}':
					return this.makeToken(Token.tokens.RIGHTBRACE_TOKEN, "}");
				case '\t':
					return this.makeToken(Token.tokens.SPACE_TOKEN, "\t");
				case ' ': //要記得在許多狀況需忽略
					return this.makeToken(Token.tokens.SPACE_TOKEN, " ");
				case '\r':
				case '\n':
					this.currLine++;
					this.isNewLine = true;
					return this.makeToken(Token.tokens.NEWLINE_TOKEN);
				case '#':
					do {
						var d = this.reader.nextChar();
						if (d == -1) {
							break;
						}
						this.bufferStr += d;
					} while (d != '\r' && d != '\n');
					this.currLine++;
					return this.makeToken(Token.tokens.HASHTAG_TOKEN, this.bufferStr);
				default:
					this.state = Scanner.MULTISYMBOL_STATE;
					this.bufferStr = nextChar;
					return this.nextToken();
			}
			break;
		case Scanner.INTLITERAL_STATE:
			var nextChar = this.reader.nextChar();
			if (nextChar >= '0' && nextChar <= '9' || nextChar == '.') {
				this.bufferStr += nextChar;
				return this.nextToken();
			} else { //讀取數字結束
				this.state = Scanner.START_STATE;
				//因判斷須多讀取一個字元  結束要退回
				if (nextChar != -1) {
					this.reader.retract();
				}
				return this.makeToken(Token.tokens.INTLITERAL_TOKEN, parseFloat(this.bufferStr));
			}
			break;
		case Scanner.IDENTIFIER_STATE:
			var checkList = ['sep', 'end'];
			//"print", "if", "elif", "while", "for", "range", "len", "int", "float", "bool", "str", "chr", "list", "dict", "tuple",, 'True', 'False'
			var nextChar = this.reader.nextChar();
			for (var i = 0; i < checkList.length; i++) {
				if (this.bufferStr.indexOf(checkList[i]) != -1) {
					var CheckChar = true;
					break;
				}
			}
			if (((nextChar >= 'a' && nextChar <= 'z') || (nextChar >= 'A' && nextChar <= 'Z') || (nextChar == '_') || (nextChar == '[') || (nextChar == ']') || (nextChar == ',') || (nextChar == '.') || (nextChar >= '0' && nextChar <= '9')) && (!CheckChar) && nextChar != ',') {
				this.bufferStr += nextChar;
				return this.nextToken();
			} else { //讀取字詞結束
				this.state = Scanner.START_STATE;
				// 因判斷須多讀取一個字元  結束要退回
				if (nextChar != -1) {
					this.reader.retract();
				}
			}
			switch (this.bufferStr) {
				case "True":
				case "False":
					return this.makeToken(Token.tokens.BOOLLITERAL_TOKEN, this.bufferStr);
				case "from":
					return this.makeToken(Token.tokens.FROM_TOKEN);
				case "import":
					return this.makeToken(Token.tokens.IMPORT_TOKEN);
				case "as":
					return this.makeToken(Token.tokens.AS_TOKEN);
				case "if":
					return this.makeToken(Token.tokens.IF_TOKEN);
				case "elif":
					return this.makeToken(Token.tokens.ELIF_TOKEN);
				case "else":
					return this.makeToken(Token.tokens.ELSE_TOKEN);
				case "while":
					return this.makeToken(Token.tokens.WHILE_TOKEN);
				case "for":
					return this.makeToken(Token.tokens.FOR_TOKEN);
				case "in":
					return this.makeToken(Token.tokens.IN_TOKEN);
				case "range":
					return this.makeToken(Token.tokens.RANGE_TOKEN);
				case "print":
					return this.makeToken(Token.tokens.PRINT_TOKEN);
				case "break":
					return this.makeToken(Token.tokens.BREAK_TOKEN);
				case "continue": //未實踐
					return this.makeToken(Token.tokens.CONTINUE_TOKEN);
				case 'and':
					return this.makeToken(Token.tokens.AND_TOKEN);
				case 'or':
					return this.makeToken(Token.tokens.OR_TOKEN);
				case 'not':
					return this.makeToken(Token.tokens.NOT_TOKEN);
				case 'sep':
					var c = this.reader.nextChar();
					var temp;
					this.bufferStr = "";
					c = this.skipSpace(c);
					if (c == '=') {
						var d = this.reader.nextChar();
						d = this.skipSpace(d);
						if (d == '\"' || d == '\'') {
							temp = d;
						} else {
							console.log("Error : sep= ???\" \'???");
						}
						var e = this.reader.nextChar();
						while (e != d) {
							if (e == -1) {
								break;
							}
							if (e == '\\') {
								this.bufferStr += e;
								e = this.reader.nextChar();

							}
							this.bufferStr += e;
							e = this.reader.nextChar();
						}
						return this.makeToken(Token.tokens.SEP_TOKEN, d + this.bufferStr);
					} else {
						console.log("Error : sep ???=???");
					}
					return this.makeToken(Token.tokens.END_TOKEN);
				case 'end':
					var c = this.reader.nextChar();
					var temp;
					this.bufferStr = "";
					c = this.skipSpace(c);
					if (c == '=') {
						var d = this.reader.nextChar();
						d = this.skipSpace(d);
						if (d == '\"' || d == '\'') {
							temp = d;
						} else {
							console.log("Error : end= ???\" \'???");
						}
						var e = this.reader.nextChar();
						while (e != d) {
							if (e == -1) {
								break;
							}
							if (e == '\\') {
								this.bufferStr += e;
								e = this.reader.nextChar();

							}
							this.bufferStr += e;
							e = this.reader.nextChar();
						}
						return this.makeToken(Token.tokens.END_TOKEN, d + this.bufferStr);
					} else {
						console.log("Error : end ???=???");
					}
					return this.makeToken(Token.tokens.END_TOKEN);
				default:
					return this.makeToken(Token.tokens.IDENTIFIER_TOKEN, this.bufferStr);
			}
			break;
		case Scanner.MULTISYMBOL_STATE:
			this.state = Scanner.START_STATE;
			switch (this.bufferStr) {
				case '=':
					var c = this.reader.nextChar();
					if (c == '=') { //==
						return this.makeToken(Token.tokens.EQUAL_TOKEN);
					} else { //=
						this.reader.retract();
						return this.makeToken(Token.tokens.ASSIGN_TOKEN);
					}
					break;
				case '+':
					var c = this.reader.nextChar();
					if (c == '=') { //+=
						return this.makeToken(Token.tokens.PLUSASSIGN_TOKEN);
					} else { //+
						this.reader.retract();
						return this.makeToken(Token.tokens.PLUS_TOKEN);
					}
					break;
				case '-':
					var c = this.reader.nextChar();
					if (c == '=') { //-=
						return this.makeToken(Token.tokens.MINUSASSIGN_TOKEN);
					} else { //-
						this.reader.retract();
						return this.makeToken(Token.tokens.MINUS_TOKEN);
					}
					break;
				case '*':
					var c = this.reader.nextChar();
					if (c == '=') { //*=
						return this.makeToken(Token.tokens.MULTASSIGN_TOKEN);
					} else if (c == '*') { //**
						return this.makeToken(Token.tokens.POWER_TOKEN);
					} else { //*
						this.reader.retract();
						return this.makeToken(Token.tokens.MULT_TOKEN);
					}
					break;
				case '/':
					var c = this.reader.nextChar();
					if (c == '=') { // /=
						return this.makeToken(Token.tokens.DIVASSIGN_TOKEN);
					} else if (c == '/') { // //
						var d = this.reader.nextChar();
						if (d == '=') { // //=
							return this.makeToken(Token.tokens.DIVFLOORASSIGN_TOKEN_TOKEN);
						}
						this.reader.retract();
						return this.makeToken(Token.tokens.DIVFLOOR_TOKEN);
					} else { // /
						this.reader.retract();
						return this.makeToken(Token.tokens.DIV_TOKEN);
					}
					break;
				case '!':
					var c = this.reader.nextChar();
					if (c == '=') { //!=
						return this.makeToken(Token.tokens.NOTEQUAL_TOKEN);
					} else { //!
						this.reader.retract();
						return this.makeToken(Token.tokens.EXCLAMATION_TOKEN);
					}
					break;
				case '%':
					var c = this.reader.nextChar();
					if (c == '=') { //%=
						return this.makeToken(Token.tokens.MODASSIGN_TOKEN);
					} else { //%
						this.reader.retract();
						return this.makeToken(Token.tokens.MOD_TOKEN);
					}
					break;
				case '>':
					var c = this.reader.nextChar();
					if (c == '=') { //>=
						return this.makeToken(Token.tokens.GREATEREQUAL_TOKEN);
					} else { //>
						this.reader.retract();
						return this.makeToken(Token.tokens.GREATER_TOKEN);
					}
					break;
				case '<':
					var c = this.reader.nextChar();
					if (c == '=') { //>=
						return this.makeToken(Token.tokens.LESSEQUAL_TOKEN);
					} else { //>
						this.reader.retract();
						return this.makeToken(Token.tokens.LESS_TOKEN);
					}
					break;
				case '\'':
					this.state = Scanner.SLASH_STATE;
					return this.nextToken();
				case '\"':
					this.state = Scanner.SLASH_STATE;
					return this.nextToken();
			}
			break;
		case Scanner.TAB_STATE:
			var nextChar = this.reader.nextChar();
			if (nextChar == ' ' || nextChar == '\t') { // 若下個還是空白
				//判斷是空白還是\T
				if (nextChar == '\t') {
					this.tabType = 0;
				} else {
					this.tabType = 1;
				}
				this.tabCount += 1;
				return this.nextToken();
			} else { // 連續空白結束
				this.state = Scanner.START_STATE;
				this.reader.retract();
				if (!this.Tab && this.tabCount > 0) { //當 Tab為0 且 tabCount大於0 才執行(第一次縮排結束)
					this.Tab = this.tabCount;
				}
				this.isNewLine = false;
				if (this.tabCount && this.Tab) {
					this.lastTab = Math.round(this.tabCount / this.Tab);
				} else {
					this.lastTab = 0;
				}
				this.tabCount = 0;
				return this.nextToken();
			}
			break;
		case Scanner.SLASH_STATE:
			this.state = Scanner.START_STATE;
			switch (this.bufferStr) {
				case '\'': // 第一個 '
					var a = this.reader.nextChar();
					if (a == '\'') { //第二個
						var n = this.reader.nextChar();
						if (n == '\'') { // ' 多行註解
							this.bufferStr = '';
							while (true) {
								var d = this.reader.nextChar();
								if (d == '\'') { //尾端第一個
									var e = this.reader.nextChar();
									if (e == '\'') { //尾端第二個}
										var f = this.reader.nextChar();
										if (f == '\'') { //尾端第三個}
											return this.makeToken(Token.tokens.BLOCKCOMMENT_TOKEN, this.bufferStr);
										} else {
											this.bufferStr += d;
											this.bufferStr += e;
											this.bufferStr += f;
										}
									} else {
										this.bufferStr += d;
										this.bufferStr += e;
									}
								} else {
									this.bufferStr += d;

									if (d == '\r' || d == '\n') {
										this.currLine++;
									}
								}
							}
						} else { // 單純空字串
							this.bufferStr += a;
							return this.makeToken(Token.tokens.QUOTE_TOKEN, this.bufferStr);
						}
					}
					//處理字串
					this.bufferStr += a;
					do {
						var str = this.reader.nextChar();
						//處理跳脫字元 \n 之類的先不處理
						if (str == '\\') {
							this.bufferStr += str;
							str = this.reader.nextChar();
							this.bufferStr += str;
							str = this.reader.nextChar();
						}
						this.bufferStr += str;
					}
					while (str != '\'');
					return this.makeToken(Token.tokens.QUOTE_TOKEN, this.bufferStr);
					break;
				case '\"':
					var a = this.reader.nextChar();
					if (a == '\"') { //第二個
						var n = this.reader.nextChar();
						if (n == '\"') { // ' 多行註解
							this.bufferStr = '';
							while (true) {
								var d = this.reader.nextChar();
								if (d == '\"') { //尾端第一個
									var e = this.reader.nextChar();
									if (e == '\"') { //尾端第二個}
										var f = this.reader.nextChar();
										if (f == '\"') { //尾端第三個}
											return this.makeToken(Token.tokens.BLOCKCOMMENT_TOKEN, this.bufferStr);
										} else {
											this.bufferStr += d;
											this.bufferStr += e;
											this.bufferStr += f;
										}
									} else {
										this.bufferStr += d;
										this.bufferStr += e;
									}
								} else {
									this.bufferStr += d;
									if (d == '\r' || d == '\n') {
										this.currLine++;
									}
								}
							}
						} else { // 單純空字串
							this.bufferStr += a;
							return this.makeToken(Token.tokens.DOUBLEQUOTE_TOKEN, this.bufferStr);
						}
					}
					//處理字串
					this.bufferStr += a;
					do {
						var str = this.reader.nextChar();
						//處理跳脫字元 \n 之類的先不處理
						if (str == '\\') {
							this.bufferStr += str;
							str = this.reader.nextChar();
							this.bufferStr += str;
							str = this.reader.nextChar();
						}
						this.bufferStr += str;
					}
					while (str != '\"');
					return this.makeToken(Token.tokens.DOUBLEQUOTE_TOKEN, this.bufferStr);
					break;
			}
			break;
		default:
			break;
	}
}

Scanner.prototype.skipSpace = function (c) {
	while (c == ' ') {
		c = this.reader.nextChar();
	}
	return c;
}
