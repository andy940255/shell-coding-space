/* eslint-disable */
//Token class
//type: Token's type
//text: the actual text that makes this token, may be null if it is not important

/* 定義Token */
function Token(type, text) {
	this.type = type;
	this.text = text;
}
/* 定義關鍵字 */
Token.tokens = {};
Token.tokens.EOS_TOKEN = 1; //end of stream
// using + 1 allows adding a new token easily later
Token.tokens.COLON_TOKEN = Token.tokens.EOS_TOKEN + 1; //:
Token.tokens.SEMICOLON_TOKEN = Token.tokens.COLON_TOKEN + 1; //;
Token.tokens.LEFTPAREN_TOKEN = Token.tokens.SEMICOLON_TOKEN + 1; //(
Token.tokens.RIGHTPAREN_TOKEN = Token.tokens.LEFTPAREN_TOKEN + 1; //)
Token.tokens.LEFTTBRACKET_TOKEN = Token.tokens.RIGHTPAREN_TOKEN + 1; //[
Token.tokens.RIGHTBRACKET_TOKEN = Token.tokens.LEFTTBRACKET_TOKEN + 1; //]
Token.tokens.LEFTBRACE_TOKEN = Token.tokens.RIGHTBRACKET_TOKEN + 1; //{
Token.tokens.RIGHTBRACE_TOKEN = Token.tokens.LEFTBRACE_TOKEN + 1; //}

Token.tokens.QUOTE_TOKEN = Token.tokens.RIGHTBRACE_TOKEN + 1; // '
Token.tokens.DOUBLEQUOTE_TOKEN = Token.tokens.QUOTE_TOKEN + 1; // "
Token.tokens.COMMA_TOKEN = Token.tokens.DOUBLEQUOTE_TOKEN + 1; // ,
Token.tokens.PERIOD_TOKEN = Token.tokens.COMMA_TOKEN + 1; // .

Token.tokens.ASSIGN_TOKEN = Token.tokens.PERIOD_TOKEN + 1; // =
Token.tokens.GREATER_TOKEN = Token.tokens.ASSIGN_TOKEN + 1; // >
Token.tokens.LESS_TOKEN = Token.tokens.GREATER_TOKEN + 1; // <
Token.tokens.EQUAL_TOKEN = Token.tokens.LESS_TOKEN + 1; // ==
Token.tokens.GREATEREQUAL_TOKEN = Token.tokens.EQUAL_TOKEN + 1; // >=
Token.tokens.LESSEQUAL_TOKEN = Token.tokens.GREATEREQUAL_TOKEN + 1; // <=

Token.tokens.STRING_TOKEN = Token.tokens.LESSEQUAL_TOKEN + 1; // 一堆字

Token.tokens.PLUS_TOKEN = Token.tokens.STRING_TOKEN + 1; //+
Token.tokens.MINUS_TOKEN = Token.tokens.PLUS_TOKEN + 1; //-
Token.tokens.MULT_TOKEN = Token.tokens.MINUS_TOKEN + 1; //*
Token.tokens.DIV_TOKEN = Token.tokens.MULT_TOKEN + 1; // /
Token.tokens.DIVFLOOR_TOKEN = Token.tokens.DIV_TOKEN + 1; // //
Token.tokens.MOD_TOKEN = Token.tokens.DIVFLOOR_TOKEN + 1; // %
Token.tokens.EXCLAMATION_TOKEN = Token.tokens.MOD_TOKEN + 1; // !

Token.tokens.PLUSASSIGN_TOKEN = Token.tokens.EXCLAMATION_TOKEN + 1; //+=
Token.tokens.MINUSASSIGN_TOKEN = Token.tokens.PLUSASSIGN_TOKEN + 1; //-=
Token.tokens.MULTASSIGN_TOKEN = Token.tokens.MINUSASSIGN_TOKEN + 1; //*=
Token.tokens.DIVASSIGN_TOKEN = Token.tokens.MULTASSIGN_TOKEN + 1; // /=
Token.tokens.DIVFLOORASSIGN_TOKEN = Token.tokens.DIVASSIGN_TOKEN + 1; // //=
Token.tokens.MODASSIGN_TOKEN = Token.tokens.DIVFLOORASSIGN_TOKEN + 1; // %=
Token.tokens.NOTEQUAL_TOKEN = Token.tokens.MODASSIGN_TOKEN + 1; //!=
Token.tokens.POWER_TOKEN = Token.tokens.NOTEQUAL_TOKEN + 1; //**

Token.tokens.PRINT_TOKEN = Token.tokens.POWER_TOKEN + 1; // print
Token.tokens.END_TOKEN = Token.tokens.PRINT_TOKEN + 1; // print-end
Token.tokens.SEP_TOKEN = Token.tokens.END_TOKEN + 1; // print-sep

Token.tokens.BOOLLITERAL_TOKEN = Token.tokens.SEP_TOKEN + 1; //true false
Token.tokens.IF_TOKEN = Token.tokens.BOOLLITERAL_TOKEN + 1; //if
Token.tokens.ELSE_TOKEN = Token.tokens.IF_TOKEN + 1; //else
Token.tokens.WHILE_TOKEN = Token.tokens.ELSE_TOKEN + 1; //while
Token.tokens.ELIF_TOKEN = Token.tokens.WHILE_TOKEN + 1; //elif
Token.tokens.FOR_TOKEN = Token.tokens.ELIF_TOKEN + 1; //for
Token.tokens.IN_TOKEN = Token.tokens.FOR_TOKEN + 1; //in
Token.tokens.RANGE_TOKEN = Token.tokens.IN_TOKEN + 1; //range

Token.tokens.IDENTIFIER_TOKEN = Token.tokens.RANGE_TOKEN + 1; //id 
Token.tokens.INTLITERAL_TOKEN = Token.tokens.IDENTIFIER_TOKEN + 1; // int number
Token.tokens.SPACE_TOKEN = Token.tokens.INTLITERAL_TOKEN + 1; //空白

Token.tokens.AND_TOKEN = Token.tokens.SPACE_TOKEN + 1; //  and
Token.tokens.OR_TOKEN = Token.tokens.AND_TOKEN + 1; // or
Token.tokens.NOT_TOKEN = Token.tokens.OR_TOKEN + 1; // not
Token.tokens.BREAK_TOKEN = Token.tokens.NOT_TOKEN + 1; // break
Token.tokens.CONTINUE_TOKEN = Token.tokens.BREAK_TOKEN + 1; // continue
Token.tokens.IMPORT_TOKEN = Token.tokens.CONTINUE_TOKEN + 1; // import
Token.tokens.FROM_TOKEN = Token.tokens.IMPORT_TOKEN + 1; // from
Token.tokens.AS_TOKEN = Token.tokens.IMPORT_TOKEN + 1; // as

Token.tokens.HASHTAG_TOKEN = Token.tokens.AS_TOKEN + 1; // # (//)
Token.tokens.BLOCKCOMMENT_TOKEN = Token.tokens.HASHTAG_TOKEN + 1; // '''  ...  ''' 或 """ ... """(/*  .... */)
Token.tokens.ERROR_TOKEN = Token.tokens.BLOCKCOMMENT_TOKEN + 1; // 語法錯誤
Token.tokens.NEWLINE_TOKEN = Token.tokens.ERROR_TOKEN + 1; // \n \r
Token.tokens.TAB_TOKEN = Token.tokens.NEWLINE_TOKEN + 1; //tab
Token.tokens.TEST_TOKEN = Token.tokens.TAB_TOKEN + 1; //tab
Token.backwardMap = {}; //for inverse look-up
for (var x in Token.tokens) {
	Token.backwardMap[Token.tokens[x]] = x;
}
