/* eslint-disable no-undef */

function initialize() {
    // marker 座標
    var position = {
        lat: 22.788954,
        lng: 120.405580
    };

    var mapOptions = {
        center: position, // 地圖中心座標
        zoom: 15, // 起始縮放大小 (0:可視範圍最大，數字遞增，可視範圍變小)
        zoomControl: false, // 關掉縮放按鈕
        fullscreenControl: false, // 關掉全屏按鈕
        // 夜間模式 沒付費所以只會出現一瞬間 -A-
        styles: [
            {
                elementType: 'geometry',
                stylers: [{
                    color: '#242f3e'
				}]
			},
            {
                elementType: 'labels.text.stroke',
                stylers: [{
                    color: '#242f3e'
				}]
			},
            {
                elementType: 'labels.text.fill',
                stylers: [{
                    color: '#746855'
				}]
			},
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{
                    color: '#d59563'
				}]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{
                    color: '#d59563'
				}]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{
                    color: '#263c3f'
				}]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{
                    color: '#6b9a76'
				}]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{
                    color: '#38414e'
				}]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{
                    color: '#212a37'
				}]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{
                    color: '#9ca5b3'
				}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{
                    color: '#746855'
				}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{
                    color: '#1f2835'
				}]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{
                    color: '#f3d19c'
				}]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{
                    color: '#2f3948'
				}]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{
                    color: '#d59563'
				}]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{
                    color: '#17263c'
				}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{
                    color: '#515c6d'
				}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{
                    color: '#17263c'
				}]
            }
          ]
    };

    // 建立地圖畫面 在"#map"
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // 標記
    var marker = new google.maps.Marker({
        position: position, // marker 座標
        icon: { // 標記圖案(預設有四種)
            path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
            scale: 10 // marker大小
        },
        map: map // 標記在哪個地圖
    });


    // 用來切換地圖
    var check = 1; // 1 和平, 0 燕巢 
    var c = 0;
    $('#changeMap').click(function () {
        if (check) {
            position = {
                lat: 22.625756,
                lng: 120.320417
            };
            check = 0;
            c++;
            $('#nknu').html("高雄師範大學-和平校區");
        } else {
            position = {
                lat: 22.788954,
                lng: 120.405580
            };
            check = 1;
            // 純屬搞笑 記得刪除
            if (c % 4 == 3)
                $('#nknu').html("燕巢師範大學-唯一校區");
            else
                $('#nknu').html("高雄師範大學-燕巢校區");
        }
        mapOptions.center = position;
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        marker = new google.maps.Marker({
            position: position, // marker 座標
            icon: { // 標記圖案(預設有四種)
                path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                scale: 10 // marker大小
            },
            map: map // 標記在哪個地圖
        });

    });
}

google.maps.event.addDomListener(window, 'load', initialize);
