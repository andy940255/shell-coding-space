/* eslint-disable */

$(document).ready(function () {
	// 新增 VueJS 處理器
	var vm = new Vue({
		el: '.centerbody',
		data: {
			title: 'Drag and Drop',
			block1: {
				value: '',
				color: ''
			},
			block2: {
				value: '',
				color: ''
			},
			block3: {
				value: '',
				color: ''
			},
		},
	});

	// 取的三個可移動方塊
	var block1 = $('#block1')[0];
	var block2 = $('#block2')[0];
	var block3 = $('#block3')[0];

	// *方塊* 建立 dragstart 監聽器
	block1.addEventListener("dragstart", dragstart, false);
	block2.addEventListener("dragstart", dragstart, false);
	block3.addEventListener("dragstart", dragstart, false);

	// *區域* 建立 drag 所需要之監聽器
	var boxteam1 = $('.boxteam1');
	for (var i = 0; i < boxteam1.length; i++) {
		boxteam1[i].addEventListener("dragover", allowDrop, false);
		boxteam1[i].addEventListener("dragenter", dragenter1, false);
		boxteam1[i].addEventListener("dragleave", dragleave1, false);
		boxteam1[i].addEventListener("drop", drop, false);
	}

	// *方塊* 開始拖曳
	function dragstart(e) {
		e.dataTransfer.setData("text", e.currentTarget.id);
	}

	// *方塊* 拖曳進入可放置區域時
	function dragenter1(e) {
		if (e.currentTarget.className.indexOf('blue') != -1) {
			e.currentTarget.style.background = "#007af4";
		} else if (e.currentTarget.className.indexOf('red') != -1) {
			e.currentTarget.style.background = "#ff7979";
		} else if (e.currentTarget.className.indexOf('yellow') != -1) {
			e.currentTarget.style.background = "#ffc21e";
		}
	}

	// *方塊* 拖曳離開可放置區域時
	function dragleave1(e) {
		if (e.currentTarget.className.indexOf('blue') != -1) {
			e.currentTarget.style.background = "blue";
		} else if (e.currentTarget.className.indexOf('red') != -1) {
			e.currentTarget.style.background = "red";
		} else if (e.currentTarget.className.indexOf('yellow') != -1) {
			e.currentTarget.style.background = "yellow";
		}
	}

	// *方塊* 結束拖曳
	function dragend1(e) {}


	// *區域* 當方塊經過區域時
	function allowDrop(e) {
		e.preventDefault();
	}

	// *區域* 當方塊放下時
	function drop(e) {
		e.preventDefault();

		var data = e.dataTransfer.getData("text");
		var div = document.getElementById(data);
		var block = whichBlock(data);
		var color;
		// 只有當拖曳對象為 blockteam1 的一員時才繼續以下內容
		if (div.className.indexOf('blockteam1') != -1) {
			if (e.currentTarget.className.indexOf('blue') != -1) {
				e.currentTarget.style.background = "blue";
				color = "Blue area\n";
			} else if (e.currentTarget.className.indexOf('red') != -1) {
				e.currentTarget.style.background = "red";
				color = "Red area\n";
			} else if (e.currentTarget.className.indexOf('yellow') != -1) {
				e.currentTarget.style.background = "yellow";
				color = "Yellow area\n";
			}

			if (e.currentTarget.childElementCount > 0) {
				var childElement = e.currentTarget.children[0];
				switchBlock(childElement.id, block);
				div.parentElement.appendChild(childElement);
			}
			chooseBlock(block, color);
			e.currentTarget.appendChild(document.getElementById(data));
		}

	}

	// 將 Block (block) 的 color 給 要交換的 Block (by id)
	function switchBlock(id, block) {
		// 若欲交換之 Block color 為 預設方塊，則給予 Empty area
		block.color = (block.color == '') ? "Empty area\n" : block.color;
		switch (id) {
			case 'block1':
				vm.block1.color = block.color;
				break;
			case 'block2':
				vm.block2.color = block.color;
				break;
			case 'block3':
				vm.block3.color = block.color;
				break;
		}
	}

	// 辨識當前拖曳的是哪個 Block，並將拖曳目的地的 color area (color) 交給當前 Block (block)
	function chooseBlock(block, color) {
		switch (block) {
			case vm.block1:
				block.value = "Aqua Block in ";
				break;
			case vm.block2:
				block.value = "Purple Block in ";
				break;
			case vm.block3:
				block.value = "Green Block in ";
				break;
		}
		block.color = color;
	}

	// 以 id (data) 尋找 Block
	function whichBlock(data) {
		switch (data) {
			case 'block1':
				return vm.block1;
			case 'block2':
				return vm.block2;
			case 'block3':
				return vm.block3;
		}
		return null;
	}
})
/*
dragstart => drag => dragenter / dragover / dragleave / dragexit => drop => dropend
drag 於一個元素或文字選取區塊被拖曳時觸發。
dragend	於拖曳操作結束時觸發（如放開滑鼠按鍵或按下鍵盤的 escape 鍵。
dragenter 於一個元素或文字選取區塊被拖曳移動進入一個有效的放置目標時觸發。
dragexit  當一個元素不再是被選取中的拖曳元素時觸發。**No support
dragleave 於一個元素或文字選取區塊被拖曳移動離開一個有效的放置目標時觸發。
dragover  於一個元素或文字選取區塊被拖曳移動經過一個有效的放置目標時觸發
dragstart 於使用者開始拖曳一個元素或文字選取區塊時觸發。
drop 於一個元素或文字選取區塊被放置至一個有效的放置目標時觸發。*/
