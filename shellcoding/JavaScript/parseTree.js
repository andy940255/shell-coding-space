/* eslint-disable */

$(document).ready(function () {
    // expressionBlocknode 用於裝載剖析樹之結果
    var expressionBlocknode;
    // level 用於縮排(方便辨識)
    var level = 0;

    // 點擊 Parse 按鈕後執行 parsePython
    $('#parseBtn').click(parsePython);

    //用於產生剖析樹
    function parsePython() {
        dragMove('#actD');
        // get python code
        var pCode = $('#codeArea').val();

        // 開始剖析
        var reader = new Reader(pCode);
        var scanner = new Scanner(reader);
        var parser = new Parser(scanner);
        var analyser = new Analyser();

        // 將剖析結果放入 expressionBlocknode
        expressionBlocknode = parser.parse();
        // 方便在 console 中觀看
        console.log(expressionBlocknode);


        analyser.evaluateExpressionBlockNode(expressionBlocknode);
        analyser.vars += 'stop';

        var umlAct = document.getElementById("actD_area");
        umlAct.removeAttribute("src");
        umlAct.setAttribute('uml', analyser.vars);
        analyser.vars = 'start\n';

        plantuml_runonce(); //更新UML用的

        // 呼叫 parseDom 將剖析樹結果解析至頁面中
        parseDom();
        // 呼叫 showActDiagram 將活動圖放置到頁面上
        showActDiagram();
    }

    // 將剖析樹結果解析至 id 為 ptDom 的 div 中
    function parseDom() {
        // 先清空該 div
        $('#ptDom').empty();
        // 用於裝載所有 node (array)
        var nodeList = [];
        // 用於裝載所有 node (object)
        var dom = document.createElement("div");

        // 使用迭代將所有 Node 放入 List (nodeList)中
        expressionBlocknode.iterate(function (express, index) {
            nodeList.push(express);
        });
        // 最外層之 node name
        var temp = document.createElement("h2");
        // 賦值並放入 tempdiv 中
        temp.innerHTML = "Expressions";
        dom.appendChild(temp);

        // 將 nodeList 中的 node 依序加入 dom 中
        for (var i in nodeList) {
            // 將展開後的物件加入 dom 中
            dom.appendChild(parseArray(nodeList[i]));
        }
        // 將 dom 加入 ptDom
        $('#ptDom')[0].appendChild(dom);
    }

    // 將 node 內物件 依序加入 tempDiv 中，並回傳 tempDiv
    // 用於展開物件所有內容
    function parseArray(node) {
        level++;
        //創立一個Div放置多個<p>
        var tempDiv = document.createElement("div");
        var temph2 = document.createElement("p");
        var str = '';
        for (var c = 0; c < level; c++) {
            str += '&thinsp;*&thinsp;';
        }
        temph2.innerHTML = str + node.constructor.name;
        tempDiv.append(temph2);
        // 遍歷 node 
        for (var i in node) {

            if (i == 'constructor') {
                continue;
            } else if (i == 'line' || i == 'iterate' || i == 'push') {
                break;
            }
            // 若 node[i] 仍為 Array 或 Node，繼續遍歷
            if (node[i] instanceof Array || node[i] instanceof Node) {
                tempDiv.appendChild(parseArray(node[i]));
            } else { // 若否，則加入 Dom 中
                var temp = document.createElement("p");
                node[i] = (node[i] == undefined) ? null : node[i];
                temp.innerHTML = str + '[ ' + i + ' ] :' + node[i];
                tempDiv.appendChild(temp);
            }
        }
        level--;
        return tempDiv;
    }

    function showActDiagram() {

    }
});
/*
範例程式碼

import math,ipaddress as ip
import socket

num1 = 100
num2 = 0
sum = num1 + num2 -100**2
if (sum <= 0):
    print("small")
elif (sum == 0):    
    print("0")
elif (sum == 10):
    print("10")
else:
    print("big")

while(sum <= 0):
    sum += 1
    break

print(sum,sep=" ",end=' ')
print(sum, sep=" ", end=' ')

sum=10
if (sum <= 0):
    print("小")
elif (sum == 0):
    print("0")
    if (sum == 0):
        print("Zero")
    elif (sum != 0):
        print("Not Zero")
elif (sum == 10):
    print("10")
else:
    print("大")

if(1):
    print("if1")
    if(2-1):
        print("if2-1")
        print("if2-1 end")
    print("if1 mid")
    if(2-2):
        print("if2-2")
        print("if2-2 end")
    print("if1 end")
print("END")

for i in range(10):
    print(i)
for i in range(1,10):
    print(i)
for i in range(1,10,):
    print(i)
for i in range(1,10,2):
    print(i)

for i in range(1):
    for j in range(2):
        print(2)
for k in range(3):
    print(3)
	*/

/*
if (a==b):
  print("a==b")
elif(1):
 print(1)
elif(1):
 print(1)
elif(1):
 print(1)
elif(1):
 print(1)
elif(1):
 print(1)
elif(1):
 print(1)
elif(1):
 print(1)
elif(1):
 print(1)
*/
