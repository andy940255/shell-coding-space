/* eslint-disable */
function init() {

	// 解答
	var ansCheck = false; // 未解答
	$("#ansbtn").click(function () {
		var list = $(".ans"); //獲取所有解答內容<p>
		console.log(list);
		if (ansCheck) { // 若已經解答，則隱藏答案
			ansCheck = false; // 改為未解答
			for (var i = 0; i < list.length; i++) { // 遍歷所有解答內容
				list[i].style.visibility = 'hidden'; // 將其隱藏
			}
		} else { // 若尚未解答，則顯示答案
			ansCheck = true; // 改為已解答
			for (var i = 0; i < list.length; i++) { // 遍歷所有解答內容
				list[i].style.visibility = 'visible'; // 將其顯示
			}
		}
	});

	// 顏色轉換
	$("#ca01").mouseover(function () { // 新增圖片監聽器
		this.onmousemove = function (e) { // 將滑鼠目前距離瀏覽器之座標 X 及 Y轉變為RGB代碼
			var parent = e.target.parentElement; // 獲取圖片之parent DIV
			parent.style.backgroundColor = "rgba(" + e.offsetX + ", " + parseInt(Math.abs(e.offsetX + e.offsetY) / 2) + ", " + e.offsetY + ")"; // 改變DIV背景顏色
		}
	});

	// 顏色轉換-恢復背景
	$("#BGcolor").mouseout(function () { // 新增圖片監聽器
		this.style.backgroundColor = null; // 將背景設為null
	});

	// 圖片置換
	$("#changeimg").mouseover(function () { // 新增圖片監聽器
		this.firstChild.setAttribute("src", "image/runnn.gif");
	});

	// 圖片置換-恢復圖片
	$("#changeimg").mouseout(function () { // 新增圖片監聽器
		this.firstChild.setAttribute("src", "image/capoo01.jpg");
	});

	// 鼠標追隨(有距離)
	var list = new Array(5); // 此為保留先前座標之陣列
	list.fill([0, 0]); // 先全數歸零
	var count = 0; // 計算目前位於陣列第幾項
	var flyCheck = false; // 確認是否在追蹤
	$("#ca03").mouseenter(function () { // 新增圖片監聽器
		document.body.onmousemove = function (e) { // 新增body監聽器
			console.log("WTF");
			var div = $("#flydiv")[0]; // 獲取將移動圖片之DIV
			div.style.display = "block"; // 顯示該圖片
			var intX = e.pageX; // 獲取pageX
			var intY = e.pageY; // 獲取pageY
			list[count] = [intX, intY]; // 將其以陣列方式存入
			var a = (count) % 5; // 目前動作之座標組
			var b = (count + 2) % 5; // 往前五個動作之座標組
			var x = 0; // X偏差值
			var y = 0; // Y偏差值
			if (list[a][0] > list[b][0]) { // 往右邊
				div.style.transform = "scaleX(-1)"; // 圖片轉向
				x = -290;
				y = -300;
			} else { // 往左邊
				div.style.transform = "scaleX(1)"; // 轉向復原
				x = -20;
				y = -300;
			}
			div.style.left = (intX + x) + "px"; // 設定圖片X座標位置
			div.style.top = (intY + y) + "px"; // 設定圖片Y座標位置
			count++; // 陣列位置數字加1
			count %= 5; // 循環陣列項目
		}
	});

	// 鼠標追隨-關閉(再次移入)
	$("#ca03").mouseout(function () {
		if (!flyCheck) { // 基數次移入(開啟追蹤)
			flyCheck = true; // 設為追蹤中
		} else { // 偶數次移入(關閉追蹤)
			flyCheck = false; // 設為不追蹤
			var div = $("#flydiv")[0]; // 獲取圖片
			div.style.display = "none"; // 將其隱藏
			document.body.onmousemove = null; // 關閉監聽器
		}
	});
	// 將寬度歸零及還原
	$("#btn5").click(function () {
		$("#ca05").animate({
			width: 'toggle'
		});
	});
	// 將圖片歸零及還原
	$("#btn6").click(function () {
		$("#ca06").toggle('slow');
	});
	// 圖片高度增加
	$("#btn7-S").click(function () {
		$("#ca07").animate({
			height: "-=100px"
		});
	});
	// 圖片高度減少
	$("#btn7-B").click(function () {
		$("#ca07").animate({
			height: "+=100px"
		});
	});

	//canvas
	var canvas = $('#myCanvas')[0];
	canvas.addEventListener('mousedown', mouseDown);
	canvas.addEventListener('mousemove', mouseMove);
	canvas.addEventListener('mouseup', mouseUp);

	$('#btn8').click(clearCanvas);

	function mouseDown(e) {
		this.draw = true; // 開始點擊滑鼠狀態
		this.ctx = this.getContext("2d");
		this.ctx.strokeStyle = '#00ebff';
		this.ctx.lineWidth = 2;

		var o = this; // canvas本身
		this.offsetX = this.offsetLeft; // 與父類別物件左方的距離
		this.offsetY = this.offsetTop; // 與父類別物件上方的距離
		//		// .offsetParent 父類別參照物( Default 為 body，依據 position 判斷)
		while (o.offsetParent) { // 找最外層
			o = o.offsetParent;
			this.offsetX += o.offsetLeft;
			this.offsetY += o.offsetTop;
		}
		// beginPath 產生一個新路徑，產生後再使用繪圖指令來設定路徑( 不使用也能繪圖，但會無法刪除已繪製之路徑)
		this.ctx.beginPath();
		// e.pageX - this.offsetX 當前滑鼠X"絕對"座標
		// moveTo 移動畫筆到指定的(x, y)座標點
		this.ctx.moveTo(e.pageX - this.offsetX, e.pageY - this.offsetY);
	}

	function mouseMove(e) {
		if (this.draw) { // 若為點擊滑鼠狀態
			// lineTo 從目前繪圖點畫一條直線到指定的(x, y)座標點
			this.ctx.lineTo(e.pageX - this.offsetX, e.pageY - this.offsetY);
			// stroke 畫出圖形的邊框
			this.ctx.stroke();
		}
	}

	function mouseUp(e) {
		this.draw = false; // 結束點擊滑鼠狀態
	}

	function clearCanvas() {
		var canvas = $('#myCanvas')[0];
		var ctx = canvas.getContext("2d");
		// 清空一個矩形的範圍
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	}

}

window.addEventListener("load", init, false); // 等待網頁載入完成後再執行
