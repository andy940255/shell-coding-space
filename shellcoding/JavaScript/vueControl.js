/* eslint-disable */

$(document).ready(function () {
	// 新增 VueJS 處理器
	var vm = new Vue({
		// 綁定 ID / CLASS
		el: '#app',
		// 數據綁定(任何數據皆須在此先宣告)
		data: {
			title: "Vue.Js",
			capooPP: '',
			message: '',
			firstname: '',
			lastname: '',
			genderWhich: 'Third gender',
			genderList: [
				{
					id: "male",
					item: 'Male'
				},
				{
					id: "female",
					item: 'Female'
				},
				{
					id: "Tgender",
					item: 'Third gender'
				},
			],
			lovefruit: [],
			fruitList: [{
				id: "apple",
				value: "Apple"
			}, {
				id: "banana",
				value: "Banana"
			}, {
				id: "coconut",
				value: "Coconut"
			}, {
				id: "durian",
				value: "Durian"
			}],
			morefruit: "",
		},
		methods: {
			// 獲取 Morefruit 內容 並加入至 fruitList
			getMorefruit: function () {
				// 防止加入空值
				if (this.morefruit == null || this.morefruit == '')
					return;
				// 確保字首為大寫，其餘為小寫
				this.morefruit = this.morefruit.substr(0, 1).toUpperCase() + this.morefruit.substr(1).toLowerCase();
				// 防止加入重複的值
				for (var i in this.fruitList) {
					if (this.fruitList[i].value == this.morefruit)
						return;
				}
				// 若通過上列所有防呆則加入 fruitList 中
				this.fruitList.push({
					id: this.morefruit.toLowerCase(),
					value: this.morefruit
				});
			},
			// 插入及收回 圖片(Tag)
			getCapoo: function () {
				if (this.capooPP == '') {
					this.capooPP = '<img src="image/CAPOOPP.png">';
				} else {
					this.capooPP = '';
				}
			}
		},
		computed: {
			// 動態更新 fullname
			fullname: function () {
				return this.firstname + ' ' + this.lastname;
			}
		}
	})
	// 定義一個名為 brand-cpnt 的新組件，用於處理LOGO的src path
	Vue.component('brand-cpnt', {
		props: ['brandSrc'],
		template: '<a class="brand" href="index.html"><img :src="brandSrc"><img class="love" src="https://pic.90sjimg.com/design/01/50/45/23/5914594f02cd7.png"></a>'
	});

	// 定義一個名為 menu-components 的新組件，用於處理及封裝頂層固定式列表
	Vue.component('menu-components', {
		props: ['post'],
		template: '<div class="menu menu-components">\
							<brand-cpnt :brandSrc="post.brandSrc"></brand-cpnt>\
						<nav>\
							<ul>\
								<li><a href="./index.html">{{ post.atitle[0] }}</a></li>\
								<li><a href="./profile.html">{{ post.atitle[1] }}</a>\
									<ul>\
										<li v-for="tag in post.aTag_About"><a :href="tag.path">{{tag.value}}</a></li>\
									</ul>\
								</li>\
								<li><a href="./project.html">{{ post.atitle[2] }}</a>\
									<ul>\
										<li v-for="tag in post.aTag_Project"><a :href="tag.path">{{tag.value}}</a></li>\
									</ul>\
								</li>\
								<li><a href="./vuejs.html">{{ post.atitle[3] }}</a>\
									<ul>\
										<li v-for="tag in post.aTag_Portfolio"><a :href="tag.path">{{tag.value}}</a></li>\
									</ul>\
								</li>\
								<li><a href="./capoo.html">{{ post.atitle[4] }}</a>\
								</li>\
								<li><a href="./contact.html">{{ post.atitle[5] }}</a>\
								</li>\
							</ul>\
						</nav>\
					</div>'
	})

	var topMenu = new Vue({
		el: '#components-demo',
		data: {
			post: {
				//				brandSrc: "https://scontent.fkhh1-1.fna.fbcdn.net/v/t1.0-9/83516446_1422521624602129_1178632214351509713_o.jpg?_nc_cat=101&_nc_sid=730e14&_nc_ohc=J04Mo3KCxEgAX_iSG0O&_nc_ht=scontent.fkhh1-1.fna&oh=dbaf598410de6ef23ba36b63b4f6a90b&oe=5F1580AF",
				brandSrc: "https://s.tcdn.co/d85/864/d85864e8-97d5-4471-8b0e-4ff5ae0338e6/192/1.png",
				atitle: ["Home", "About", "Project", "Portfolio", "My favorite character", "Contact", ],
				aTag_About: [{
					path: "./profile.html",
					value: "Profile"
				}],
				aTag_Project: [{
					path: "./AP2/index.html",
					value: "Airpollution 2"
				}, {
					path: "https://140.127.74.13/myBlock/",
					value: "myBlock(HTML)"
							   }, {
					path: "https://sw-hie-ie.nknu.edu.tw:8006/",
					value: "myBlock(Node.JS)"
							   }],
				aTag_Portfolio: [{
					path: "./vuejs.html",
					value: "Vue.js"
				}, {
					path: "./dragdemo.html",
					value: "DragDemo"
				}, {
					path: "./parseTree.html",
					value: "ParseTree"
				}],
			}
		}
	});
})
