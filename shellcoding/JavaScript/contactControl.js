/* eslint-disable */

$(document).ready(function () {
	// Mail Submit
	$('#mailSubmit').click(function () {
		console.log("Submit");
		// 使用 const 預防修改可能
		const title = $('#eTitle').val();
		const context = $('#eContext').val();
		// 給予長度讓後台區別 title 與 context
		const data = title.length + ' ' + context.length + ' ' + title + ' ' + context;
		// post {title & context} to contact.html; return json
		$.ajax({
			type: "POST",
			url: '/contact.html',
			data: data,
			success: function (response) {
				console.log("post success");
				console.log(response);

				$('#eTitle').val("");
				$('#eContext').val("");

				alert('Submit Success');
			},
			dataType: 'json'
		});
	});
});
