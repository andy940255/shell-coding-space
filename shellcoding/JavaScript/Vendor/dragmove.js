/* eslint-disable */
//map scrolling

function dragMove(ID) {
	//map scrolling
	var slider = document.querySelector(ID);
	var isDown = false;
	var startX;
	var startY;
	var scrollLeft;
	var scrollTop;

	slider.addEventListener('mousedown', (e) => {
		isDown = true;
		slider.classList.add("active");
		startX = e.pageX - slider.offsetLeft;
		startY = e.pageY - slider.offsetTop;
		scrollTop = slider.scrollTop;
		scrollLeft = slider.scrollLeft;
	});

	slider.addEventListener('mouseleave', () => {
		isDown = false;
		slider.classList.remove("active");
	});

	slider.addEventListener('mouseup', () => {
		isDown = false;
		slider.classList.remove("active");
	});

	slider.addEventListener('mousemove', (e) => {
		if (!isDown) return;
		e.preventDefault();
		var x = e.pageX - slider.offsetLeft;
		var walkX = (x - startX);
		var y = e.pageY - slider.offsetTop;
		var walkY = (y - startY);
		//&& scrollTop - walkY <= 1300
		if (scrollTop - walkY >= 0) {
			slider.scrollTop = scrollTop - walkY;
		}
		//&& scrollLeft - walkX <= 2000
		if (scrollLeft - walkX >= 0) {
			slider.scrollLeft = scrollLeft - walkX;
		}
	});
}
