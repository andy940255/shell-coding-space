var player; // Youtube API
var playerStatus; // 

function onYouTubeIframeAPIReady() {

	player = new YT.Player("video-container", {
		videoId: 'PSjaPznKprc', // Youtube 影片ID
		width: '540 ', // 播放器寬度 (px)
		height: '360', // 播放器高度 (px)
		//playerVars: {
		//			autoplay: 1, // 自動播放影片
		//			controls: 1, // 顯示播放器
		//			showinfo: 0, // 隱藏影片標題
		//			modestbranding: 0, // 隱藏YouTube Logo
		//			loop: 1, // 重覆播放
		//			playlist: 'PSjaPznKprc', // 當使用影片要重覆播放時，需再輸入YouTube 影片ID
		//			fs: 0, // 隱藏全螢幕按鈕
		//			cc_load_policty: 0, // 隱藏字幕
		//			iv_load_policy: 3, // 設置為 1 - 顯示視頻註釋，設置為 3 則不顯示，default:1
		//			autohide: 0 // 影片播放時，隱藏影片控制列
		//		},
		events: {
			// 'onApiChange': ...  當API啟動時(開始播放)
			'onReady': onPlayerReady, // 當API準備好時
			'onStateChange': onPlayerStateChange // 當狀態改變時(play, pause, stop)
			/*
			六種狀態
			-1 (unstarted)
			0 (ended)
			1 (playing)
			2 (paused)
			3 (buffering)
			5 (video cued)
			*/
		}
	});

	$('#play').click(function () {
		// 播放影片
		if (playerStatus != YT.PlayerState.PLAYING) {
			player.playVideo();
		}
	});

	$('#pause').click(function () {
		// 暫停影片
		if (playerStatus == YT.PlayerState.PLAYING) {
			player.pauseVideo(); // 'player' variable we already defined and assigned.
		}
	});
	$('#stop').click(function () {
		// 影片重整
		if (playerStatus != YT.PlayerState.ENDED) {
			player.stopVideo();
		}
	});

	$('#mute').click(function () {
		// 靜音及取消靜音
		if (player.isMuted()) { // 如果偵測到靜音中
			player.unMute(); // 解除靜音
			$(this).css('text-decoration', 'none'); // 取消刪除線
		} else { // 如果偵測到沒有靜音
			player.mute(); // 設為靜音
			$(this).css('text-decoration', 'line-through'); // 加上刪除線
		}
	});

	// 藉由 input(slider)來調整影片音量
	$('#slider')[0].oninput = function () {
		player.setVolume(this.value);
	}

}

function onPlayerReady(event) {
	setInterval(function () {
		// 同步音量
		$('#slider')[0].value = event.target.getVolume();
		//同步靜音
		if (player.isMuted()) { // 如果偵測到靜音中
			$('#mute').css('text-decoration', 'line-through'); // 加上刪除線
		} else { // 如果偵測到沒有靜音
			$('#mute').css('text-decoration', 'none'); // 取消刪除線
		}
	}, 200);
}

function onPlayerStateChange(event) { // 將目前影片狀態丟給 playerStatus
	playerStatus = event.data;
	console.log(playerStatus);
}
